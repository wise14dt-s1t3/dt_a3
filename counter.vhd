-- Code belongs to WiSe14DT-S1T3

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity counter is																	-- Device Under test
	port (
		did		: out	std_logic_vector(  2 downto 0 );	-- Dut IDentifier
		err		: out	std_logic;												-- ERRor: invalid counter value
		cnt		: out	std_logic_vector( 11 downto 0 );	-- CouNT
		--
		valLd	: in	std_logic_vector( 11 downto 0 );	-- init VALue in case of LoaD
		nLd		: in	std_logic;												-- Not Load; low active LoaD
		up		: in	std_logic;												-- UP count command
		down	: in	std_logic;												-- DOWN count command
		--
		clk		: in	std_logic;												-- CLocK
		nres	: in	std_logic													-- Not RESet; low active synchronous reset
	);--]port
end entity counter;
