-- Code belongs to WiSe14DT-S1T3

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.numeric_std.all;


architecture counter_arop of counter is
	signal did_ns	: std_logic_vector( 2 downto 0 ) := (others => '0');
	signal err_ns	: std_logic := '0';
	signal cnt_ns	: std_logic_vector( 12 downto 0 ) := (others => '0');

	signal did_cs	: std_logic_vector( 2 downto 0 ) := (others => '0');
	signal err_cs	: std_logic := '0';
	signal cnt_cs	: std_logic_vector( 11 downto 0 ) := (others => '0');

	signal valLd_cs	: std_logic_vector( 11 downto 0 ) := (others => '0');
	signal nLd_cs	: std_logic := '0';
	signal up_cs	: std_logic := '0';
	signal down_cs	: std_logic := '0';

	signal cur_s	: std_logic_vector( 12 downto 0 ) := (others => '0'); -- current counter value before up/down
	signal add_s	: std_logic_vector( 12 downto 0 ) := (others => '0'); -- add to current counter
begin
	did <= did_cs;
	err <= err_cs;
	cnt <= cnt_cs;

	reg:
	process (clk) is
	begin
		if clk='1' and clk'event then
			if nres='0' then
				did_cs <= (others => '0');
				err_cs <= '0';
				cnt_cs <= (others => '0');

				valLd_cs <= (others => '0');
				nLd_cs <= '0';
				up_cs <= '0';
				down_cs <= '0';
			else
				did_cs <= did_ns;
				err_cs <= err_ns;
				cnt_cs <= cnt_ns( 11 downto 0 );

				valLd_cs <= valLd;
				nLd_cs <= nLd;
				up_cs <= up;
				down_cs <= down;
			end if;
		end if;
	end process reg;

	getCur:
	process (valLd_cs, nLd_cs, cnt_cs) is
		variable cur_v : std_logic_vector( 11 downto 0 );
	begin
		if (nLd_cs = '1') then
			cur_v := cnt_cs;
		else
			cur_v := valLd_cs;
		end if;

		cur_s <= '0' & cur_v;
	end process getCur;

	getAddVal:
	process (up_cs, down_cs) is
		variable compare : std_logic_vector ( 1 downto 0 );
		variable add_v : std_logic_vector( 12 downto 0 );
	begin
		compare := up_cs & down_cs;

		case compare is
			when "01" => -- down
				add_v := (others => '1'); -- -1
			when "10" => -- up
				add_v := (0 => '1', others => '0');
			when others => -- "00" "11"
				add_v := (others => '0');
		end case;

		add_s <= add_v;
	end process getAddVal;

	did_ns <= (2 => '1', others => '0');

	add:
	process (cur_s, add_s) is
		variable cnt_v : std_logic_vector( 12 downto 0 );
	begin
		cnt_v := cur_s + add_s;

		cnt_ns <= cnt_v;
	end process add;

	error:
	process (cur_s, add_s, cnt_ns) is
		variable cnt_positiv_v : std_logic;
		variable cur_positiv_v : std_logic;
		variable add_positiv_v : std_logic;
		variable vect_v : std_logic_vector( 2 downto 0 );
		variable err_v : std_logic;
	begin
		cnt_positiv_v := cnt_ns(cnt_ns'high-1);
		cur_positiv_v := cur_s(cur_s'high-1);
		add_positiv_v := add_s(add_s'high-1);

		vect_v := cur_positiv_v & add_positiv_v & cnt_positiv_v;

		case vect_v is
			when "001" => --Carry
				err_v := '1';
			when "110" => --Borrow
				err_v := '1';
			when others =>
				err_v := '0';
		end case;
		err_ns <= err_v;
	end process error;

end architecture counter_arop;
