-- Code belongs to WiSe14DT-S1T3

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


architecture dut of counter is 

	component dut
		port(
			did	: out std_logic_vector( 2 downto 0 );
			err	: out std_logic;
			cnt	: out std_logic_vector( 11 downto 0 );
			--
			valLd	: in std_logic_vector( 11 downto 0 );
			nLd	: in std_logic;
			up	: in std_logic;
			down	: in std_logic;
			--
			clk	: in std_logic;
			nres	: in std_logic
		);--]port
	end component dut;
	for all : dut use entity work.counter( counter_argl );

begin

	dut_i : dut
		port map (
			did => did,
			err => err,
			cnt => cnt,
			--
			valLd => valLd,
			nLd => nLd,
			up => up,
			down => down,
			--
			clk => clk,
			nres => nres
		)--]port
	;--}dut_i

end architecture dut;
