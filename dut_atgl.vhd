--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: counter_timesim.vhd
-- /___/   /\     Timestamp: Fri Dec 05 13:48:18 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm counter -w -dir netgen/fit -ofmt vhdl -sim counter.nga counter_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: counter.nga
-- Output file	: D:\DT\ISE_a03\netgen\fit\counter_timesim.vhd
-- # of Entities	: 1
-- Design Name	: counter.nga
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity counter is
  port (
    clk : in STD_LOGIC := 'X'; 
    nres : in STD_LOGIC := 'X'; 
    down : in STD_LOGIC := 'X'; 
    up : in STD_LOGIC := 'X'; 
    nLd : in STD_LOGIC := 'X'; 
    err : out STD_LOGIC; 
    valLd : in STD_LOGIC_VECTOR ( 11 downto 0 ); 
    cnt : out STD_LOGIC_VECTOR ( 11 downto 0 ); 
    did : out STD_LOGIC_VECTOR ( 2 downto 0 ) 
  );
end counter;

architecture Structure of counter is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal down_II_UIM_5 : STD_LOGIC; 
  signal up_II_UIM_7 : STD_LOGIC; 
  signal nLd_II_UIM_9 : STD_LOGIC; 
  signal valLd_10_II_UIM_11 : STD_LOGIC; 
  signal valLd_8_II_UIM_13 : STD_LOGIC; 
  signal valLd_9_II_UIM_15 : STD_LOGIC; 
  signal valLd_7_II_UIM_17 : STD_LOGIC; 
  signal valLd_1_II_UIM_19 : STD_LOGIC; 
  signal valLd_0_II_UIM_21 : STD_LOGIC; 
  signal valLd_2_II_UIM_23 : STD_LOGIC; 
  signal valLd_3_II_UIM_25 : STD_LOGIC; 
  signal valLd_6_II_UIM_27 : STD_LOGIC; 
  signal valLd_5_II_UIM_29 : STD_LOGIC; 
  signal valLd_4_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal cnt_0_MC_Q_35 : STD_LOGIC; 
  signal cnt_10_MC_Q_37 : STD_LOGIC; 
  signal cnt_11_MC_Q_39 : STD_LOGIC; 
  signal cnt_1_MC_Q_41 : STD_LOGIC; 
  signal cnt_2_MC_Q_43 : STD_LOGIC; 
  signal cnt_3_MC_Q_45 : STD_LOGIC; 
  signal cnt_4_MC_Q_47 : STD_LOGIC; 
  signal cnt_5_MC_Q_49 : STD_LOGIC; 
  signal cnt_6_MC_Q_51 : STD_LOGIC; 
  signal cnt_7_MC_Q_53 : STD_LOGIC; 
  signal cnt_8_MC_Q_55 : STD_LOGIC; 
  signal cnt_9_MC_Q_57 : STD_LOGIC; 
  signal did_0_MC_Q_59 : STD_LOGIC; 
  signal did_1_MC_Q_61 : STD_LOGIC; 
  signal did_2_MC_Q_63 : STD_LOGIC; 
  signal err_MC_Q_65 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_UIM_67 : STD_LOGIC; 
  signal cnt_0_MC_D_68 : STD_LOGIC; 
  signal Gnd_69 : STD_LOGIC; 
  signal Vcc_70 : STD_LOGIC; 
  signal cnt_0_MC_D1_71 : STD_LOGIC; 
  signal cnt_0_MC_D2_72 : STD_LOGIC; 
  signal N_PZ_211_73 : STD_LOGIC; 
  signal nLd_cs_74 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_0_75 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_1_77 : STD_LOGIC; 
  signal N_PZ_211_MC_Q_78 : STD_LOGIC; 
  signal N_PZ_211_MC_D_79 : STD_LOGIC; 
  signal N_PZ_211_MC_D1_80 : STD_LOGIC; 
  signal N_PZ_211_MC_D2_81 : STD_LOGIC; 
  signal down_cs_82 : STD_LOGIC; 
  signal up_cs_83 : STD_LOGIC; 
  signal carry_v_84 : STD_LOGIC; 
  signal N_PZ_211_MC_D2_PT_0_85 : STD_LOGIC; 
  signal N_PZ_211_MC_D2_PT_1_86 : STD_LOGIC; 
  signal down_cs_MC_Q : STD_LOGIC; 
  signal down_cs_MC_D_88 : STD_LOGIC; 
  signal down_cs_MC_D1_89 : STD_LOGIC; 
  signal down_cs_MC_D2_90 : STD_LOGIC; 
  signal up_cs_MC_Q : STD_LOGIC; 
  signal up_cs_MC_D_92 : STD_LOGIC; 
  signal up_cs_MC_D1_93 : STD_LOGIC; 
  signal up_cs_MC_D2_94 : STD_LOGIC; 
  signal carry_v_MC_Q_95 : STD_LOGIC; 
  signal carry_v_MC_D_96 : STD_LOGIC; 
  signal carry_v_MC_D1_97 : STD_LOGIC; 
  signal carry_v_MC_D2_98 : STD_LOGIC; 
  signal N_PZ_123_99 : STD_LOGIC; 
  signal carry_v_MC_D2_PT_0_100 : STD_LOGIC; 
  signal cnt_10_MC_UIM_101 : STD_LOGIC; 
  signal cnt_9_MC_UIM_102 : STD_LOGIC; 
  signal carry_v_or0009_103 : STD_LOGIC; 
  signal cnt_7_BUFR_104 : STD_LOGIC; 
  signal cnt_8_BUFR_105 : STD_LOGIC; 
  signal cnt_11_MC_UIM_106 : STD_LOGIC; 
  signal carry_v_MC_D2_PT_1_107 : STD_LOGIC; 
  signal carry_v_MC_D2_PT_2_113 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_10_MC_D_115 : STD_LOGIC; 
  signal cnt_10_MC_D1_116 : STD_LOGIC; 
  signal cnt_10_MC_D2_117 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_0_118 : STD_LOGIC; 
  signal N_PZ_132_119 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_1_120 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_2_121 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_3_122 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_4_123 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_5_124 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_6_125 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_7_126 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_8_127 : STD_LOGIC; 
  signal nLd_cs_MC_Q : STD_LOGIC; 
  signal nLd_cs_MC_D_129 : STD_LOGIC; 
  signal nLd_cs_MC_D1_130 : STD_LOGIC; 
  signal nLd_cs_MC_D2_131 : STD_LOGIC; 
  signal valLd_cs_10_MC_Q : STD_LOGIC; 
  signal valLd_cs_10_MC_D_133 : STD_LOGIC; 
  signal valLd_cs_10_MC_D1_134 : STD_LOGIC; 
  signal valLd_cs_10_MC_D2_135 : STD_LOGIC; 
  signal valLd_cs_8_MC_Q : STD_LOGIC; 
  signal valLd_cs_8_MC_D_137 : STD_LOGIC; 
  signal valLd_cs_8_MC_D1_138 : STD_LOGIC; 
  signal valLd_cs_8_MC_D2_139 : STD_LOGIC; 
  signal valLd_cs_9_MC_Q : STD_LOGIC; 
  signal valLd_cs_9_MC_D_141 : STD_LOGIC; 
  signal valLd_cs_9_MC_D1_142 : STD_LOGIC; 
  signal valLd_cs_9_MC_D2_143 : STD_LOGIC; 
  signal N_PZ_123_MC_Q_144 : STD_LOGIC; 
  signal N_PZ_123_MC_D_145 : STD_LOGIC; 
  signal N_PZ_123_MC_D1_146 : STD_LOGIC; 
  signal N_PZ_123_MC_D2_147 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_9_MC_D_149 : STD_LOGIC; 
  signal cnt_9_MC_D1_150 : STD_LOGIC; 
  signal cnt_9_MC_D2_151 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_0_152 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_1_153 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_2_154 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_3_155 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_4_156 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_5_157 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_6_158 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_7_159 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_8_160 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_9_161 : STD_LOGIC; 
  signal valLd_cs_7_MC_Q : STD_LOGIC; 
  signal valLd_cs_7_MC_D_163 : STD_LOGIC; 
  signal valLd_cs_7_MC_D1_164 : STD_LOGIC; 
  signal valLd_cs_7_MC_D2_165 : STD_LOGIC; 
  signal carry_v_or0009_MC_Q_166 : STD_LOGIC; 
  signal carry_v_or0009_MC_D_167 : STD_LOGIC; 
  signal carry_v_or0009_MC_D1_168 : STD_LOGIC; 
  signal carry_v_or0009_MC_D2_169 : STD_LOGIC; 
  signal carry_v_or0007_170 : STD_LOGIC; 
  signal cnt_3_MC_UIM_171 : STD_LOGIC; 
  signal cnt_6_MC_UIM_172 : STD_LOGIC; 
  signal cnt_5_BUFR_173 : STD_LOGIC; 
  signal cnt_4_BUFR_174 : STD_LOGIC; 
  signal carry_v_or0009_MC_D2_PT_0_175 : STD_LOGIC; 
  signal carry_v_or0009_MC_D2_PT_1_176 : STD_LOGIC; 
  signal carry_v_or0009_MC_D2_PT_2_181 : STD_LOGIC; 
  signal carry_v_or0009_MC_D2_PT_3_182 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_3_MC_D_184 : STD_LOGIC; 
  signal cnt_3_MC_D1_185 : STD_LOGIC; 
  signal cnt_3_MC_D2_186 : STD_LOGIC; 
  signal N_PZ_128_187 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_0_188 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_1_189 : STD_LOGIC; 
  signal N_PZ_128_MC_Q_190 : STD_LOGIC; 
  signal N_PZ_128_MC_D_191 : STD_LOGIC; 
  signal N_PZ_128_MC_D1_192 : STD_LOGIC; 
  signal N_PZ_128_MC_D2_193 : STD_LOGIC; 
  signal N_PZ_128_MC_D2_PT_0_194 : STD_LOGIC; 
  signal N_PZ_128_MC_D2_PT_1_195 : STD_LOGIC; 
  signal carry_v_or0007_MC_Q_196 : STD_LOGIC; 
  signal carry_v_or0007_MC_D_197 : STD_LOGIC; 
  signal carry_v_or0007_MC_D1_198 : STD_LOGIC; 
  signal carry_v_or0007_MC_D2_199 : STD_LOGIC; 
  signal cnt_1_MC_UIM_200 : STD_LOGIC; 
  signal N_PZ_198_201 : STD_LOGIC; 
  signal cnt_2_MC_UIM_202 : STD_LOGIC; 
  signal carry_v_or0007_MC_D2_PT_0_203 : STD_LOGIC; 
  signal N_PZ_209_204 : STD_LOGIC; 
  signal carry_v_or0007_MC_D2_PT_1_205 : STD_LOGIC; 
  signal N_PZ_199_207 : STD_LOGIC; 
  signal carry_v_or0007_MC_D2_PT_2_209 : STD_LOGIC; 
  signal N_PZ_210_210 : STD_LOGIC; 
  signal carry_v_or0007_MC_D2_PT_3_211 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_213 : STD_LOGIC; 
  signal cnt_1_MC_D1_214 : STD_LOGIC; 
  signal cnt_1_MC_D2_215 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_216 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_217 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_2_218 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_3_219 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_4_220 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_5_221 : STD_LOGIC; 
  signal N_PZ_198_MC_Q_222 : STD_LOGIC; 
  signal N_PZ_198_MC_D_223 : STD_LOGIC; 
  signal N_PZ_198_MC_D1_224 : STD_LOGIC; 
  signal N_PZ_198_MC_D2_225 : STD_LOGIC; 
  signal valLd_cs_1_MC_Q : STD_LOGIC; 
  signal valLd_cs_1_MC_D_227 : STD_LOGIC; 
  signal valLd_cs_1_MC_D1_228 : STD_LOGIC; 
  signal valLd_cs_1_MC_D2_229 : STD_LOGIC; 
  signal N_PZ_199_MC_Q_230 : STD_LOGIC; 
  signal N_PZ_199_MC_D_231 : STD_LOGIC; 
  signal N_PZ_199_MC_D1_232 : STD_LOGIC; 
  signal N_PZ_199_MC_D2_233 : STD_LOGIC; 
  signal valLd_cs_0_MC_Q : STD_LOGIC; 
  signal valLd_cs_0_MC_D_235 : STD_LOGIC; 
  signal valLd_cs_0_MC_D1_236 : STD_LOGIC; 
  signal valLd_cs_0_MC_D2_237 : STD_LOGIC; 
  signal N_PZ_209_MC_Q_238 : STD_LOGIC; 
  signal N_PZ_209_MC_D_239 : STD_LOGIC; 
  signal N_PZ_209_MC_D1_240 : STD_LOGIC; 
  signal N_PZ_209_MC_D2_241 : STD_LOGIC; 
  signal N_PZ_209_MC_D2_PT_0_242 : STD_LOGIC; 
  signal N_PZ_209_MC_D2_PT_1_243 : STD_LOGIC; 
  signal N_PZ_209_MC_D2_PT_2_244 : STD_LOGIC; 
  signal N_PZ_210_MC_Q_245 : STD_LOGIC; 
  signal N_PZ_210_MC_D_246 : STD_LOGIC; 
  signal N_PZ_210_MC_D1_247 : STD_LOGIC; 
  signal N_PZ_210_MC_D2_248 : STD_LOGIC; 
  signal N_PZ_210_MC_D2_PT_0_249 : STD_LOGIC; 
  signal N_PZ_210_MC_D2_PT_1_250 : STD_LOGIC; 
  signal N_PZ_210_MC_D2_PT_2_251 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_253 : STD_LOGIC; 
  signal cnt_2_MC_D1_254 : STD_LOGIC; 
  signal cnt_2_MC_D2_255 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_256 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_257 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_258 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_3_259 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_4_260 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_5_261 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_6_262 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_7_263 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_8_264 : STD_LOGIC; 
  signal valLd_cs_2_MC_Q : STD_LOGIC; 
  signal valLd_cs_2_MC_D_266 : STD_LOGIC; 
  signal valLd_cs_2_MC_D1_267 : STD_LOGIC; 
  signal valLd_cs_2_MC_D2_268 : STD_LOGIC; 
  signal valLd_cs_3_MC_Q : STD_LOGIC; 
  signal valLd_cs_3_MC_D_270 : STD_LOGIC; 
  signal valLd_cs_3_MC_D1_271 : STD_LOGIC; 
  signal valLd_cs_3_MC_D2_272 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_6_MC_D_274 : STD_LOGIC; 
  signal cnt_6_MC_D1_275 : STD_LOGIC; 
  signal cnt_6_MC_D2_276 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_0_277 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_1_278 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_2_279 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_3_280 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_4_281 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_5_282 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_6_283 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_7_284 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_8_285 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_9_286 : STD_LOGIC; 
  signal valLd_cs_6_MC_Q : STD_LOGIC; 
  signal valLd_cs_6_MC_D_288 : STD_LOGIC; 
  signal valLd_cs_6_MC_D1_289 : STD_LOGIC; 
  signal valLd_cs_6_MC_D2_290 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D_292 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D1_293 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_294 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_0_295 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_1_296 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_2_297 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_3_298 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_4_299 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_5_300 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_6_301 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_7_302 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_8_303 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_9_304 : STD_LOGIC; 
  signal valLd_cs_5_MC_Q : STD_LOGIC; 
  signal valLd_cs_5_MC_D_306 : STD_LOGIC; 
  signal valLd_cs_5_MC_D1_307 : STD_LOGIC; 
  signal valLd_cs_5_MC_D2_308 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D_310 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D1_311 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_312 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_0_313 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_1_314 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_2_315 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_3_316 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_4_317 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_5_318 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_6_319 : STD_LOGIC; 
  signal valLd_cs_4_MC_Q : STD_LOGIC; 
  signal valLd_cs_4_MC_D_321 : STD_LOGIC; 
  signal valLd_cs_4_MC_D1_322 : STD_LOGIC; 
  signal valLd_cs_4_MC_D2_323 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D_325 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D1_326 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_327 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_0_328 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_1_329 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_2_330 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_3_331 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_4_332 : STD_LOGIC; 
  signal N_PZ_132_MC_Q_333 : STD_LOGIC; 
  signal N_PZ_132_MC_D_334 : STD_LOGIC; 
  signal N_PZ_132_MC_D1_335 : STD_LOGIC; 
  signal N_PZ_132_MC_D2_336 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D_338 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D1_339 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_340 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_0_341 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_1_342 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_2_343 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_3_344 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_4_345 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_5_346 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_6_347 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_11_MC_D_349 : STD_LOGIC; 
  signal cnt_11_MC_D1_350 : STD_LOGIC; 
  signal cnt_11_MC_D2_351 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_0_352 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_1_353 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_2_354 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_3_355 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_4_356 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_5_357 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_6_358 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_7_359 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_8_360 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_9_361 : STD_LOGIC; 
  signal valLd_cs_11_MC_Q : STD_LOGIC; 
  signal valLd_cs_11_MC_D_363 : STD_LOGIC; 
  signal valLd_cs_11_MC_D1_364 : STD_LOGIC; 
  signal valLd_cs_11_MC_D2_365 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q_366 : STD_LOGIC; 
  signal cnt_4_MC_D_367 : STD_LOGIC; 
  signal cnt_4_MC_D1_368 : STD_LOGIC; 
  signal cnt_4_MC_D2_369 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q_370 : STD_LOGIC; 
  signal cnt_5_MC_D_371 : STD_LOGIC; 
  signal cnt_5_MC_D1_372 : STD_LOGIC; 
  signal cnt_5_MC_D2_373 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q_374 : STD_LOGIC; 
  signal cnt_7_MC_D_375 : STD_LOGIC; 
  signal cnt_7_MC_D1_376 : STD_LOGIC; 
  signal cnt_7_MC_D2_377 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q_378 : STD_LOGIC; 
  signal cnt_8_MC_D_379 : STD_LOGIC; 
  signal cnt_8_MC_D1_380 : STD_LOGIC; 
  signal cnt_8_MC_D2_381 : STD_LOGIC; 
  signal did_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal did_0_MC_D_383 : STD_LOGIC; 
  signal did_0_MC_D1_384 : STD_LOGIC; 
  signal did_0_MC_D2_385 : STD_LOGIC; 
  signal did_1_MC_Q_tsimrenamed_net_Q_386 : STD_LOGIC; 
  signal did_1_MC_D_387 : STD_LOGIC; 
  signal did_1_MC_D1_388 : STD_LOGIC; 
  signal did_1_MC_D2_389 : STD_LOGIC; 
  signal did_2_MC_Q_tsimrenamed_net_Q_390 : STD_LOGIC; 
  signal did_2_MC_D_391 : STD_LOGIC; 
  signal did_2_MC_D1_392 : STD_LOGIC; 
  signal did_2_MC_D2_393 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_D_395 : STD_LOGIC; 
  signal err_MC_D1_396 : STD_LOGIC; 
  signal err_MC_D2_397 : STD_LOGIC; 
  signal err_MC_D2_PT_0_398 : STD_LOGIC; 
  signal err_MC_D2_PT_1_399 : STD_LOGIC; 
  signal err_MC_D2_PT_2_400 : STD_LOGIC; 
  signal err_MC_D2_PT_3_401 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_8_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_123_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_123_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_123_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_123_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_9_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0009_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_128_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_128_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_128_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_128_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_128_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_128_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_128_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_128_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_v_or0007_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_198_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_198_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_198_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_198_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_199_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_199_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_199_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_199_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_209_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_210_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_8_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_9_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_9_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_123_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0009_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_128_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_128_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0007_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0007_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0007_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0007_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_v_or0007_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_198_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_198_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_199_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_199_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_209_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_209_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_209_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_209_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_210_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_210_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_210_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_210_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_8_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_9_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_132_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_132_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_9_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_5
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_7
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_9
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_11
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_13
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_15
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_17
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_19
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_21
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_23
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_25
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_27
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_29
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_35,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_37,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_39,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_41,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_43,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_45,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_47,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_49,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_51,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_53,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_55,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_57,
      O => cnt(9)
    );
  did_0_Q : X_BUF
    port map (
      I => did_0_MC_Q_59,
      O => did(0)
    );
  did_1_Q : X_BUF
    port map (
      I => did_1_MC_Q_61,
      O => did(1)
    );
  did_2_Q : X_BUF
    port map (
      I => did_2_MC_Q_63,
      O => did(2)
    );
  err_66 : X_BUF
    port map (
      I => err_MC_Q_65,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_35
    );
  cnt_0_MC_UIM : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_UIM_67
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_69
    );
  Vcc : X_ONE
    port map (
      O => Vcc_70
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_68
    );
  cnt_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D1_IN1,
      O => cnt_0_MC_D1_71
    );
  cnt_0_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2,
      O => cnt_0_MC_D2_PT_0_75
    );
  cnt_0_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2,
      O => cnt_0_MC_D2_PT_1_77
    );
  cnt_0_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_IN1,
      O => cnt_0_MC_D2_72
    );
  N_PZ_211 : X_BUF
    port map (
      I => N_PZ_211_MC_Q_78,
      O => N_PZ_211_73
    );
  N_PZ_211_MC_Q : X_BUF
    port map (
      I => N_PZ_211_MC_D_79,
      O => N_PZ_211_MC_Q_78
    );
  N_PZ_211_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_211_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_211_MC_D_IN1,
      O => N_PZ_211_MC_D_79
    );
  N_PZ_211_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_211_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_211_MC_D1_IN1,
      O => N_PZ_211_MC_D1_80
    );
  N_PZ_211_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN1,
      O => N_PZ_211_MC_D2_PT_0_85
    );
  N_PZ_211_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN1,
      O => N_PZ_211_MC_D2_PT_1_86
    );
  N_PZ_211_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_211_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_211_MC_D2_IN1,
      O => N_PZ_211_MC_D2_81
    );
  down_cs : X_BUF
    port map (
      I => down_cs_MC_Q,
      O => down_cs_82
    );
  down_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_down_cs_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_down_cs_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => down_cs_MC_Q
    );
  down_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_down_cs_MC_D_IN0,
      I1 => NlwBufferSignal_down_cs_MC_D_IN1,
      O => down_cs_MC_D_88
    );
  down_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_down_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_down_cs_MC_D1_IN1,
      O => down_cs_MC_D1_89
    );
  down_cs_MC_D2 : X_ZERO
    port map (
      O => down_cs_MC_D2_90
    );
  up_cs : X_BUF
    port map (
      I => up_cs_MC_Q,
      O => up_cs_83
    );
  up_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_up_cs_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_up_cs_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => up_cs_MC_Q
    );
  up_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_up_cs_MC_D_IN0,
      I1 => NlwBufferSignal_up_cs_MC_D_IN1,
      O => up_cs_MC_D_92
    );
  up_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_up_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_up_cs_MC_D1_IN1,
      O => up_cs_MC_D1_93
    );
  up_cs_MC_D2 : X_ZERO
    port map (
      O => up_cs_MC_D2_94
    );
  carry_v : X_BUF
    port map (
      I => carry_v_MC_Q_95,
      O => carry_v_84
    );
  carry_v_MC_Q : X_BUF
    port map (
      I => carry_v_MC_D_96,
      O => carry_v_MC_Q_95
    );
  carry_v_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_carry_v_MC_D_IN0,
      I1 => NlwBufferSignal_carry_v_MC_D_IN1,
      O => carry_v_MC_D_96
    );
  carry_v_MC_D1 : X_ZERO
    port map (
      O => carry_v_MC_D1_97
    );
  carry_v_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_carry_v_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_carry_v_MC_D2_PT_0_IN1,
      O => carry_v_MC_D2_PT_0_100
    );
  carry_v_MC_D2_PT_1 : X_AND7
    port map (
      I0 => NlwBufferSignal_carry_v_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_carry_v_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_carry_v_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_carry_v_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_carry_v_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_carry_v_MC_D2_PT_1_IN5,
      I6 => NlwInverterSignal_carry_v_MC_D2_PT_1_IN6,
      O => carry_v_MC_D2_PT_1_107
    );
  carry_v_MC_D2_PT_2 : X_AND7
    port map (
      I0 => NlwInverterSignal_carry_v_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_carry_v_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_carry_v_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_carry_v_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_carry_v_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_carry_v_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_carry_v_MC_D2_PT_2_IN6,
      O => carry_v_MC_D2_PT_2_113
    );
  carry_v_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_carry_v_MC_D2_IN0,
      I1 => NlwBufferSignal_carry_v_MC_D2_IN1,
      I2 => NlwBufferSignal_carry_v_MC_D2_IN2,
      O => carry_v_MC_D2_98
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_Q_37
    );
  cnt_10_MC_UIM : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_UIM_101
    );
  cnt_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_10_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_10_MC_Q_tsimrenamed_net_Q
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_115
    );
  cnt_10_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D1_IN2,
      O => cnt_10_MC_D1_116
    );
  cnt_10_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN4,
      O => cnt_10_MC_D2_PT_0_118
    );
  cnt_10_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN4,
      O => cnt_10_MC_D2_PT_1_120
    );
  cnt_10_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4,
      O => cnt_10_MC_D2_PT_2_121
    );
  cnt_10_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4,
      O => cnt_10_MC_D2_PT_3_122
    );
  cnt_10_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN4,
      O => cnt_10_MC_D2_PT_4_123
    );
  cnt_10_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN5,
      O => cnt_10_MC_D2_PT_5_124
    );
  cnt_10_MC_D2_PT_6 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_10_MC_D2_PT_6_IN5,
      I6 => NlwInverterSignal_cnt_10_MC_D2_PT_6_IN6,
      O => cnt_10_MC_D2_PT_6_125
    );
  cnt_10_MC_D2_PT_7 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN5,
      I6 => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN6,
      O => cnt_10_MC_D2_PT_7_126
    );
  cnt_10_MC_D2_PT_8 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_8_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_8_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN5,
      I6 => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN6,
      I7 => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN7,
      O => cnt_10_MC_D2_PT_8_127
    );
  cnt_10_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_10_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_10_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_10_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_10_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_10_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_10_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_10_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_10_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_10_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_10_MC_D2_IN15,
      O => cnt_10_MC_D2_117
    );
  nLd_cs : X_BUF
    port map (
      I => nLd_cs_MC_Q,
      O => nLd_cs_74
    );
  nLd_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_nLd_cs_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_nLd_cs_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => nLd_cs_MC_Q
    );
  nLd_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_nLd_cs_MC_D_IN0,
      I1 => NlwBufferSignal_nLd_cs_MC_D_IN1,
      O => nLd_cs_MC_D_129
    );
  nLd_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_nLd_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_nLd_cs_MC_D1_IN1,
      O => nLd_cs_MC_D1_130
    );
  nLd_cs_MC_D2 : X_ZERO
    port map (
      O => nLd_cs_MC_D2_131
    );
  valLd_cs_10_Q : X_BUF
    port map (
      I => valLd_cs_10_MC_Q,
      O => valLd_cs(10)
    );
  valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_10_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_10_MC_Q
    );
  valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_10_MC_D_IN1,
      O => valLd_cs_10_MC_D_133
    );
  valLd_cs_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_10_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_10_MC_D1_IN1,
      O => valLd_cs_10_MC_D1_134
    );
  valLd_cs_10_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_10_MC_D2_135
    );
  valLd_cs_8_Q : X_BUF
    port map (
      I => valLd_cs_8_MC_Q,
      O => valLd_cs(8)
    );
  valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_8_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_8_MC_Q
    );
  valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_8_MC_D_IN1,
      O => valLd_cs_8_MC_D_137
    );
  valLd_cs_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_8_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_8_MC_D1_IN1,
      O => valLd_cs_8_MC_D1_138
    );
  valLd_cs_8_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_8_MC_D2_139
    );
  valLd_cs_9_Q : X_BUF
    port map (
      I => valLd_cs_9_MC_Q,
      O => valLd_cs(9)
    );
  valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_9_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_9_MC_Q
    );
  valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_9_MC_D_IN1,
      O => valLd_cs_9_MC_D_141
    );
  valLd_cs_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_9_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_9_MC_D1_IN1,
      O => valLd_cs_9_MC_D1_142
    );
  valLd_cs_9_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_9_MC_D2_143
    );
  N_PZ_123 : X_BUF
    port map (
      I => N_PZ_123_MC_Q_144,
      O => N_PZ_123_99
    );
  N_PZ_123_MC_Q : X_BUF
    port map (
      I => N_PZ_123_MC_D_145,
      O => N_PZ_123_MC_Q_144
    );
  N_PZ_123_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_123_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_123_MC_D_IN1,
      O => N_PZ_123_MC_D_145
    );
  N_PZ_123_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_123_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_123_MC_D1_IN1,
      O => N_PZ_123_MC_D1_146
    );
  N_PZ_123_MC_D2 : X_ZERO
    port map (
      O => N_PZ_123_MC_D2_147
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_Q_57
    );
  cnt_9_MC_UIM : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_UIM_102
    );
  cnt_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_9_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_9_MC_Q_tsimrenamed_net_Q
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_149
    );
  cnt_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D1_IN1,
      O => cnt_9_MC_D1_150
    );
  cnt_9_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN2,
      O => cnt_9_MC_D2_PT_0_152
    );
  cnt_9_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN4,
      O => cnt_9_MC_D2_PT_1_153
    );
  cnt_9_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4,
      O => cnt_9_MC_D2_PT_2_154
    );
  cnt_9_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4,
      O => cnt_9_MC_D2_PT_3_155
    );
  cnt_9_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4,
      O => cnt_9_MC_D2_PT_4_156
    );
  cnt_9_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_9_MC_D2_PT_5_IN4,
      O => cnt_9_MC_D2_PT_5_157
    );
  cnt_9_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN4,
      O => cnt_9_MC_D2_PT_6_158
    );
  cnt_9_MC_D2_PT_7 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_7_IN3,
      I4 => NlwInverterSignal_cnt_9_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_9_MC_D2_PT_7_IN5,
      O => cnt_9_MC_D2_PT_7_159
    );
  cnt_9_MC_D2_PT_8 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN4,
      I5 => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN5,
      O => cnt_9_MC_D2_PT_8_160
    );
  cnt_9_MC_D2_PT_9 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_9_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN4,
      I5 => NlwInverterSignal_cnt_9_MC_D2_PT_9_IN5,
      I6 => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN6,
      O => cnt_9_MC_D2_PT_9_161
    );
  cnt_9_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_9_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_9_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_9_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_9_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_9_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_9_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_9_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_9_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_IN15,
      O => cnt_9_MC_D2_151
    );
  valLd_cs_7_Q : X_BUF
    port map (
      I => valLd_cs_7_MC_Q,
      O => valLd_cs(7)
    );
  valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_7_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_7_MC_Q
    );
  valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_7_MC_D_IN1,
      O => valLd_cs_7_MC_D_163
    );
  valLd_cs_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_7_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_7_MC_D1_IN1,
      O => valLd_cs_7_MC_D1_164
    );
  valLd_cs_7_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_7_MC_D2_165
    );
  carry_v_or0009 : X_BUF
    port map (
      I => carry_v_or0009_MC_Q_166,
      O => carry_v_or0009_103
    );
  carry_v_or0009_MC_Q : X_BUF
    port map (
      I => carry_v_or0009_MC_D_167,
      O => carry_v_or0009_MC_Q_166
    );
  carry_v_or0009_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_carry_v_or0009_MC_D_IN0,
      I1 => NlwBufferSignal_carry_v_or0009_MC_D_IN1,
      O => carry_v_or0009_MC_D_167
    );
  carry_v_or0009_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_carry_v_or0009_MC_D1_IN0,
      I1 => NlwBufferSignal_carry_v_or0009_MC_D1_IN1,
      O => carry_v_or0009_MC_D1_168
    );
  carry_v_or0009_MC_D2_PT_0 : X_AND7
    port map (
      I0 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN6,
      O => carry_v_or0009_MC_D2_PT_0_175
    );
  carry_v_or0009_MC_D2_PT_1 : X_AND7
    port map (
      I0 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN6,
      O => carry_v_or0009_MC_D2_PT_1_176
    );
  carry_v_or0009_MC_D2_PT_2 : X_AND7
    port map (
      I0 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN6,
      O => carry_v_or0009_MC_D2_PT_2_181
    );
  carry_v_or0009_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_carry_v_or0009_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN6,
      O => carry_v_or0009_MC_D2_PT_3_182
    );
  carry_v_or0009_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_carry_v_or0009_MC_D2_IN0,
      I1 => NlwBufferSignal_carry_v_or0009_MC_D2_IN1,
      I2 => NlwBufferSignal_carry_v_or0009_MC_D2_IN2,
      I3 => NlwBufferSignal_carry_v_or0009_MC_D2_IN3,
      O => carry_v_or0009_MC_D2_169
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_Q_45
    );
  cnt_3_MC_UIM : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_UIM_171
    );
  cnt_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_3_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_3_MC_Q_tsimrenamed_net_Q
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_184
    );
  cnt_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D1_IN1,
      O => cnt_3_MC_D1_185
    );
  cnt_3_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN2,
      O => cnt_3_MC_D2_PT_0_188
    );
  cnt_3_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2,
      O => cnt_3_MC_D2_PT_1_189
    );
  cnt_3_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_IN1,
      O => cnt_3_MC_D2_186
    );
  N_PZ_128 : X_BUF
    port map (
      I => N_PZ_128_MC_Q_190,
      O => N_PZ_128_187
    );
  N_PZ_128_MC_Q : X_BUF
    port map (
      I => N_PZ_128_MC_D_191,
      O => N_PZ_128_MC_Q_190
    );
  N_PZ_128_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_128_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_128_MC_D_IN1,
      O => N_PZ_128_MC_D_191
    );
  N_PZ_128_MC_D1 : X_ZERO
    port map (
      O => N_PZ_128_MC_D1_192
    );
  N_PZ_128_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_128_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_128_MC_D2_PT_0_IN1,
      O => N_PZ_128_MC_D2_PT_0_194
    );
  N_PZ_128_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_128_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_128_MC_D2_PT_1_IN1,
      O => N_PZ_128_MC_D2_PT_1_195
    );
  N_PZ_128_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_128_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_128_MC_D2_IN1,
      O => N_PZ_128_MC_D2_193
    );
  carry_v_or0007 : X_BUF
    port map (
      I => carry_v_or0007_MC_Q_196,
      O => carry_v_or0007_170
    );
  carry_v_or0007_MC_Q : X_BUF
    port map (
      I => carry_v_or0007_MC_D_197,
      O => carry_v_or0007_MC_Q_196
    );
  carry_v_or0007_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_carry_v_or0007_MC_D_IN0,
      I1 => NlwBufferSignal_carry_v_or0007_MC_D_IN1,
      O => carry_v_or0007_MC_D_197
    );
  carry_v_or0007_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_carry_v_or0007_MC_D1_IN0,
      I1 => NlwBufferSignal_carry_v_or0007_MC_D1_IN1,
      O => carry_v_or0007_MC_D1_198
    );
  carry_v_or0007_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_carry_v_or0007_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_carry_v_or0007_MC_D2_PT_0_IN4,
      O => carry_v_or0007_MC_D2_PT_0_203
    );
  carry_v_or0007_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_carry_v_or0007_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN4,
      O => carry_v_or0007_MC_D2_PT_1_205
    );
  carry_v_or0007_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN4,
      O => carry_v_or0007_MC_D2_PT_2_209
    );
  carry_v_or0007_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwInverterSignal_carry_v_or0007_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_carry_v_or0007_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN4,
      O => carry_v_or0007_MC_D2_PT_3_211
    );
  carry_v_or0007_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_carry_v_or0007_MC_D2_IN0,
      I1 => NlwBufferSignal_carry_v_or0007_MC_D2_IN1,
      I2 => NlwBufferSignal_carry_v_or0007_MC_D2_IN2,
      I3 => NlwBufferSignal_carry_v_or0007_MC_D2_IN3,
      O => carry_v_or0007_MC_D2_199
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_41
    );
  cnt_1_MC_UIM : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_UIM_200
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_213
    );
  cnt_1_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D1_IN2,
      O => cnt_1_MC_D1_214
    );
  cnt_1_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3,
      O => cnt_1_MC_D2_PT_0_216
    );
  cnt_1_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3,
      O => cnt_1_MC_D2_PT_1_217
    );
  cnt_1_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN4,
      O => cnt_1_MC_D2_PT_2_218
    );
  cnt_1_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4,
      O => cnt_1_MC_D2_PT_3_219
    );
  cnt_1_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4,
      O => cnt_1_MC_D2_PT_4_220
    );
  cnt_1_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4,
      O => cnt_1_MC_D2_PT_5_221
    );
  cnt_1_MC_D2 : X_OR6
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_1_MC_D2_IN5,
      O => cnt_1_MC_D2_215
    );
  N_PZ_198 : X_BUF
    port map (
      I => N_PZ_198_MC_Q_222,
      O => N_PZ_198_201
    );
  N_PZ_198_MC_Q : X_BUF
    port map (
      I => N_PZ_198_MC_D_223,
      O => N_PZ_198_MC_Q_222
    );
  N_PZ_198_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_198_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_198_MC_D_IN1,
      O => N_PZ_198_MC_D_223
    );
  N_PZ_198_MC_D1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_198_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_198_MC_D1_IN1,
      O => N_PZ_198_MC_D1_224
    );
  N_PZ_198_MC_D2 : X_ZERO
    port map (
      O => N_PZ_198_MC_D2_225
    );
  valLd_cs_1_Q : X_BUF
    port map (
      I => valLd_cs_1_MC_Q,
      O => valLd_cs(1)
    );
  valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_1_MC_Q
    );
  valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_1_MC_D_IN1,
      O => valLd_cs_1_MC_D_227
    );
  valLd_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_1_MC_D1_IN1,
      O => valLd_cs_1_MC_D1_228
    );
  valLd_cs_1_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_1_MC_D2_229
    );
  N_PZ_199 : X_BUF
    port map (
      I => N_PZ_199_MC_Q_230,
      O => N_PZ_199_207
    );
  N_PZ_199_MC_Q : X_BUF
    port map (
      I => N_PZ_199_MC_D_231,
      O => N_PZ_199_MC_Q_230
    );
  N_PZ_199_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_199_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_199_MC_D_IN1,
      O => N_PZ_199_MC_D_231
    );
  N_PZ_199_MC_D1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_199_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_199_MC_D1_IN1,
      O => N_PZ_199_MC_D1_232
    );
  N_PZ_199_MC_D2 : X_ZERO
    port map (
      O => N_PZ_199_MC_D2_233
    );
  valLd_cs_0_Q : X_BUF
    port map (
      I => valLd_cs_0_MC_Q,
      O => valLd_cs(0)
    );
  valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_0_MC_Q
    );
  valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_0_MC_D_IN1,
      O => valLd_cs_0_MC_D_235
    );
  valLd_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_0_MC_D1_IN1,
      O => valLd_cs_0_MC_D1_236
    );
  valLd_cs_0_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_0_MC_D2_237
    );
  N_PZ_209 : X_BUF
    port map (
      I => N_PZ_209_MC_Q_238,
      O => N_PZ_209_204
    );
  N_PZ_209_MC_Q : X_BUF
    port map (
      I => N_PZ_209_MC_D_239,
      O => N_PZ_209_MC_Q_238
    );
  N_PZ_209_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_209_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_209_MC_D_IN1,
      O => N_PZ_209_MC_D_239
    );
  N_PZ_209_MC_D1 : X_ZERO
    port map (
      O => N_PZ_209_MC_D1_240
    );
  N_PZ_209_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_209_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_209_MC_D2_PT_0_IN1,
      O => N_PZ_209_MC_D2_PT_0_242
    );
  N_PZ_209_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_209_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_209_MC_D2_PT_1_IN2,
      O => N_PZ_209_MC_D2_PT_1_243
    );
  N_PZ_209_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_209_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_N_PZ_209_MC_D2_PT_2_IN2,
      O => N_PZ_209_MC_D2_PT_2_244
    );
  N_PZ_209_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_N_PZ_209_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_209_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_209_MC_D2_IN2,
      O => N_PZ_209_MC_D2_241
    );
  N_PZ_210 : X_BUF
    port map (
      I => N_PZ_210_MC_Q_245,
      O => N_PZ_210_210
    );
  N_PZ_210_MC_Q : X_BUF
    port map (
      I => N_PZ_210_MC_D_246,
      O => N_PZ_210_MC_Q_245
    );
  N_PZ_210_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_210_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_210_MC_D_IN1,
      O => N_PZ_210_MC_D_246
    );
  N_PZ_210_MC_D1 : X_ZERO
    port map (
      O => N_PZ_210_MC_D1_247
    );
  N_PZ_210_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_210_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_210_MC_D2_PT_0_IN1,
      O => N_PZ_210_MC_D2_PT_0_249
    );
  N_PZ_210_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_210_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_210_MC_D2_PT_1_IN2,
      O => N_PZ_210_MC_D2_PT_1_250
    );
  N_PZ_210_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_210_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_N_PZ_210_MC_D2_PT_2_IN2,
      O => N_PZ_210_MC_D2_PT_2_251
    );
  N_PZ_210_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_N_PZ_210_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_210_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_210_MC_D2_IN2,
      O => N_PZ_210_MC_D2_248
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_43
    );
  cnt_2_MC_UIM : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_UIM_202
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_253
    );
  cnt_2_MC_D1 : X_ZERO
    port map (
      O => cnt_2_MC_D1_254
    );
  cnt_2_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN2,
      O => cnt_2_MC_D2_PT_0_256
    );
  cnt_2_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3,
      O => cnt_2_MC_D2_PT_1_257
    );
  cnt_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3,
      O => cnt_2_MC_D2_PT_2_258
    );
  cnt_2_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4,
      O => cnt_2_MC_D2_PT_3_259
    );
  cnt_2_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4,
      O => cnt_2_MC_D2_PT_4_260
    );
  cnt_2_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4,
      O => cnt_2_MC_D2_PT_5_261
    );
  cnt_2_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4,
      O => cnt_2_MC_D2_PT_6_262
    );
  cnt_2_MC_D2_PT_7 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN4,
      O => cnt_2_MC_D2_PT_7_263
    );
  cnt_2_MC_D2_PT_8 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_8_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN4,
      O => cnt_2_MC_D2_PT_8_264
    );
  cnt_2_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_2_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_2_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_2_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_2_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_2_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_2_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_2_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_2_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_2_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_2_MC_D2_IN15,
      O => cnt_2_MC_D2_255
    );
  valLd_cs_2_Q : X_BUF
    port map (
      I => valLd_cs_2_MC_Q,
      O => valLd_cs(2)
    );
  valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_2_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_2_MC_Q
    );
  valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_2_MC_D_IN1,
      O => valLd_cs_2_MC_D_266
    );
  valLd_cs_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_2_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_2_MC_D1_IN1,
      O => valLd_cs_2_MC_D1_267
    );
  valLd_cs_2_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_2_MC_D2_268
    );
  valLd_cs_3_Q : X_BUF
    port map (
      I => valLd_cs_3_MC_Q,
      O => valLd_cs(3)
    );
  valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_3_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_3_MC_Q
    );
  valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_3_MC_D_IN1,
      O => valLd_cs_3_MC_D_270
    );
  valLd_cs_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_3_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_3_MC_D1_IN1,
      O => valLd_cs_3_MC_D1_271
    );
  valLd_cs_3_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_3_MC_D2_272
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_Q_51
    );
  cnt_6_MC_UIM : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_UIM_172
    );
  cnt_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_6_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_6_MC_Q_tsimrenamed_net_Q
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_274
    );
  cnt_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D1_IN1,
      O => cnt_6_MC_D1_275
    );
  cnt_6_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4,
      O => cnt_6_MC_D2_PT_0_277
    );
  cnt_6_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4,
      O => cnt_6_MC_D2_PT_1_278
    );
  cnt_6_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN4,
      O => cnt_6_MC_D2_PT_2_279
    );
  cnt_6_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN4,
      O => cnt_6_MC_D2_PT_3_280
    );
  cnt_6_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4,
      O => cnt_6_MC_D2_PT_4_281
    );
  cnt_6_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN4,
      O => cnt_6_MC_D2_PT_5_282
    );
  cnt_6_MC_D2_PT_6 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN7,
      O => cnt_6_MC_D2_PT_6_283
    );
  cnt_6_MC_D2_PT_7 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN7,
      O => cnt_6_MC_D2_PT_7_284
    );
  cnt_6_MC_D2_PT_8 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN7,
      O => cnt_6_MC_D2_PT_8_285
    );
  cnt_6_MC_D2_PT_9 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_9_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_9_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_9_IN7,
      O => cnt_6_MC_D2_PT_9_286
    );
  cnt_6_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_6_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_6_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_IN15,
      O => cnt_6_MC_D2_276
    );
  valLd_cs_6_Q : X_BUF
    port map (
      I => valLd_cs_6_MC_Q,
      O => valLd_cs(6)
    );
  valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_6_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_6_MC_Q
    );
  valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_6_MC_D_IN1,
      O => valLd_cs_6_MC_D_288
    );
  valLd_cs_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_6_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_6_MC_D1_IN1,
      O => valLd_cs_6_MC_D1_289
    );
  valLd_cs_6_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_6_MC_D2_290
    );
  cnt_5_BUFR : X_BUF
    port map (
      I => cnt_5_BUFR_MC_Q,
      O => cnt_5_BUFR_173
    );
  cnt_5_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_5_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_5_BUFR_MC_Q
    );
  cnt_5_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D_IN1,
      O => cnt_5_BUFR_MC_D_292
    );
  cnt_5_BUFR_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_5_BUFR_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D1_IN2,
      O => cnt_5_BUFR_MC_D1_293
    );
  cnt_5_BUFR_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN3,
      O => cnt_5_BUFR_MC_D2_PT_0_295
    );
  cnt_5_BUFR_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN4,
      O => cnt_5_BUFR_MC_D2_PT_1_296
    );
  cnt_5_BUFR_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN4,
      O => cnt_5_BUFR_MC_D2_PT_2_297
    );
  cnt_5_BUFR_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_3_IN4,
      O => cnt_5_BUFR_MC_D2_PT_3_298
    );
  cnt_5_BUFR_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN6,
      O => cnt_5_BUFR_MC_D2_PT_4_299
    );
  cnt_5_BUFR_MC_D2_PT_5 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN4,
      I5 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_5_IN5,
      I6 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN6,
      O => cnt_5_BUFR_MC_D2_PT_5_300
    );
  cnt_5_BUFR_MC_D2_PT_6 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN5,
      I6 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN6,
      O => cnt_5_BUFR_MC_D2_PT_6_301
    );
  cnt_5_BUFR_MC_D2_PT_7 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN5,
      I6 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN6,
      O => cnt_5_BUFR_MC_D2_PT_7_302
    );
  cnt_5_BUFR_MC_D2_PT_8 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_8_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN4,
      I5 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN5,
      I6 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_8_IN6,
      O => cnt_5_BUFR_MC_D2_PT_8_303
    );
  cnt_5_BUFR_MC_D2_PT_9 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN2,
      I3 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN3,
      I4 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN4,
      I5 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN5,
      I6 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN6,
      O => cnt_5_BUFR_MC_D2_PT_9_304
    );
  cnt_5_BUFR_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN15,
      O => cnt_5_BUFR_MC_D2_294
    );
  valLd_cs_5_Q : X_BUF
    port map (
      I => valLd_cs_5_MC_Q,
      O => valLd_cs(5)
    );
  valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_5_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_5_MC_Q
    );
  valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_5_MC_D_IN1,
      O => valLd_cs_5_MC_D_306
    );
  valLd_cs_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_5_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_5_MC_D1_IN1,
      O => valLd_cs_5_MC_D1_307
    );
  valLd_cs_5_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_5_MC_D2_308
    );
  cnt_4_BUFR : X_BUF
    port map (
      I => cnt_4_BUFR_MC_Q,
      O => cnt_4_BUFR_174
    );
  cnt_4_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_4_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_4_BUFR_MC_Q
    );
  cnt_4_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_BUFR_MC_D_IN1,
      O => cnt_4_BUFR_MC_D_310
    );
  cnt_4_BUFR_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_4_BUFR_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_4_BUFR_MC_D1_IN2,
      O => cnt_4_BUFR_MC_D1_311
    );
  cnt_4_BUFR_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN4,
      O => cnt_4_BUFR_MC_D2_PT_0_313
    );
  cnt_4_BUFR_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN4,
      O => cnt_4_BUFR_MC_D2_PT_1_314
    );
  cnt_4_BUFR_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN4,
      O => cnt_4_BUFR_MC_D2_PT_2_315
    );
  cnt_4_BUFR_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN4,
      O => cnt_4_BUFR_MC_D2_PT_3_316
    );
  cnt_4_BUFR_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN4,
      O => cnt_4_BUFR_MC_D2_PT_4_317
    );
  cnt_4_BUFR_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN4,
      I5 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN5,
      O => cnt_4_BUFR_MC_D2_PT_5_318
    );
  cnt_4_BUFR_MC_D2_PT_6 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN5,
      O => cnt_4_BUFR_MC_D2_PT_6_319
    );
  cnt_4_BUFR_MC_D2 : X_OR7
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN6,
      O => cnt_4_BUFR_MC_D2_312
    );
  valLd_cs_4_Q : X_BUF
    port map (
      I => valLd_cs_4_MC_Q,
      O => valLd_cs(4)
    );
  valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_4_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_4_MC_Q
    );
  valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_4_MC_D_IN1,
      O => valLd_cs_4_MC_D_321
    );
  valLd_cs_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_4_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_4_MC_D1_IN1,
      O => valLd_cs_4_MC_D1_322
    );
  valLd_cs_4_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_4_MC_D2_323
    );
  cnt_7_BUFR : X_BUF
    port map (
      I => cnt_7_BUFR_MC_Q,
      O => cnt_7_BUFR_104
    );
  cnt_7_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_7_BUFR_MC_Q
    );
  cnt_7_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D_IN1,
      O => cnt_7_BUFR_MC_D_325
    );
  cnt_7_BUFR_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1,
      O => cnt_7_BUFR_MC_D1_326
    );
  cnt_7_BUFR_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_0_IN3,
      O => cnt_7_BUFR_MC_D2_PT_0_328
    );
  cnt_7_BUFR_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN3,
      O => cnt_7_BUFR_MC_D2_PT_1_329
    );
  cnt_7_BUFR_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN3,
      O => cnt_7_BUFR_MC_D2_PT_2_330
    );
  cnt_7_BUFR_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN4,
      O => cnt_7_BUFR_MC_D2_PT_3_331
    );
  cnt_7_BUFR_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN5,
      O => cnt_7_BUFR_MC_D2_PT_4_332
    );
  cnt_7_BUFR_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN4,
      O => cnt_7_BUFR_MC_D2_327
    );
  N_PZ_132 : X_BUF
    port map (
      I => N_PZ_132_MC_Q_333,
      O => N_PZ_132_119
    );
  N_PZ_132_MC_Q : X_BUF
    port map (
      I => N_PZ_132_MC_D_334,
      O => N_PZ_132_MC_Q_333
    );
  N_PZ_132_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_132_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_132_MC_D_IN1,
      O => N_PZ_132_MC_D_334
    );
  N_PZ_132_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_132_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_132_MC_D1_IN1,
      I2 => NlwInverterSignal_N_PZ_132_MC_D1_IN2,
      O => N_PZ_132_MC_D1_335
    );
  N_PZ_132_MC_D2 : X_ZERO
    port map (
      O => N_PZ_132_MC_D2_336
    );
  cnt_8_BUFR : X_BUF
    port map (
      I => cnt_8_BUFR_MC_Q,
      O => cnt_8_BUFR_105
    );
  cnt_8_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_8_BUFR_MC_Q
    );
  cnt_8_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D_IN1,
      O => cnt_8_BUFR_MC_D_338
    );
  cnt_8_BUFR_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN2,
      O => cnt_8_BUFR_MC_D1_339
    );
  cnt_8_BUFR_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3,
      O => cnt_8_BUFR_MC_D2_PT_0_341
    );
  cnt_8_BUFR_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN4,
      O => cnt_8_BUFR_MC_D2_PT_1_342
    );
  cnt_8_BUFR_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4,
      O => cnt_8_BUFR_MC_D2_PT_2_343
    );
  cnt_8_BUFR_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN4,
      O => cnt_8_BUFR_MC_D2_PT_3_344
    );
  cnt_8_BUFR_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN4,
      O => cnt_8_BUFR_MC_D2_PT_4_345
    );
  cnt_8_BUFR_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN4,
      O => cnt_8_BUFR_MC_D2_PT_5_346
    );
  cnt_8_BUFR_MC_D2_PT_6 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN4,
      I5 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN5,
      O => cnt_8_BUFR_MC_D2_PT_6_347
    );
  cnt_8_BUFR_MC_D2 : X_OR7
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN6,
      O => cnt_8_BUFR_MC_D2_340
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_Q_39
    );
  cnt_11_MC_UIM : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_UIM_106
    );
  cnt_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_11_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_11_MC_Q_tsimrenamed_net_Q
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_349
    );
  cnt_11_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D1_IN2,
      O => cnt_11_MC_D1_350
    );
  cnt_11_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN4,
      O => cnt_11_MC_D2_PT_0_352
    );
  cnt_11_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN4,
      O => cnt_11_MC_D2_PT_1_353
    );
  cnt_11_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4,
      O => cnt_11_MC_D2_PT_2_354
    );
  cnt_11_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4,
      O => cnt_11_MC_D2_PT_3_355
    );
  cnt_11_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN4,
      O => cnt_11_MC_D2_PT_4_356
    );
  cnt_11_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN4,
      O => cnt_11_MC_D2_PT_5_357
    );
  cnt_11_MC_D2_PT_6 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN6,
      O => cnt_11_MC_D2_PT_6_358
    );
  cnt_11_MC_D2_PT_7 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN5,
      I6 => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN6,
      I7 => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN7,
      O => cnt_11_MC_D2_PT_7_359
    );
  cnt_11_MC_D2_PT_8 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN6,
      I7 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN7,
      O => cnt_11_MC_D2_PT_8_360
    );
  cnt_11_MC_D2_PT_9 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN6,
      I7 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN7,
      I8 => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN8,
      I9 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN9,
      I10 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN10,
      I11 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN11,
      I12 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN12,
      I13 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN13,
      I14 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN15,
      O => cnt_11_MC_D2_PT_9_361
    );
  cnt_11_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_11_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_11_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_11_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_11_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_11_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_11_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_11_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_11_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_IN15,
      O => cnt_11_MC_D2_351
    );
  valLd_cs_11_Q : X_BUF
    port map (
      I => valLd_cs_11_MC_Q,
      O => valLd_cs(11)
    );
  valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_11_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_11_MC_Q
    );
  valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_11_MC_D_IN1,
      O => valLd_cs_11_MC_D_363
    );
  valLd_cs_11_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_11_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_11_MC_D1_IN1,
      O => valLd_cs_11_MC_D1_364
    );
  valLd_cs_11_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_11_MC_D2_365
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q_366,
      O => cnt_4_MC_Q_47
    );
  cnt_4_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_4_MC_D_367,
      O => cnt_4_MC_Q_tsimrenamed_net_Q_366
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_367
    );
  cnt_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D1_IN1,
      O => cnt_4_MC_D1_368
    );
  cnt_4_MC_D2 : X_ZERO
    port map (
      O => cnt_4_MC_D2_369
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q_370,
      O => cnt_5_MC_Q_49
    );
  cnt_5_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_5_MC_D_371,
      O => cnt_5_MC_Q_tsimrenamed_net_Q_370
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_371
    );
  cnt_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D1_IN1,
      O => cnt_5_MC_D1_372
    );
  cnt_5_MC_D2 : X_ZERO
    port map (
      O => cnt_5_MC_D2_373
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q_374,
      O => cnt_7_MC_Q_53
    );
  cnt_7_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_7_MC_D_375,
      O => cnt_7_MC_Q_tsimrenamed_net_Q_374
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_375
    );
  cnt_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D1_IN1,
      O => cnt_7_MC_D1_376
    );
  cnt_7_MC_D2 : X_ZERO
    port map (
      O => cnt_7_MC_D2_377
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q_378,
      O => cnt_8_MC_Q_55
    );
  cnt_8_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_8_MC_D_379,
      O => cnt_8_MC_Q_tsimrenamed_net_Q_378
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_379
    );
  cnt_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D1_IN1,
      O => cnt_8_MC_D1_380
    );
  cnt_8_MC_D2 : X_ZERO
    port map (
      O => cnt_8_MC_D2_381
    );
  did_0_MC_Q : X_BUF
    port map (
      I => did_0_MC_Q_tsimrenamed_net_Q,
      O => did_0_MC_Q_59
    );
  did_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_did_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_did_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => did_0_MC_Q_tsimrenamed_net_Q
    );
  did_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_0_MC_D_IN0,
      I1 => NlwBufferSignal_did_0_MC_D_IN1,
      O => did_0_MC_D_383
    );
  did_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_did_0_MC_D1_IN0,
      I1 => NlwBufferSignal_did_0_MC_D1_IN1,
      O => did_0_MC_D1_384
    );
  did_0_MC_D2 : X_ZERO
    port map (
      O => did_0_MC_D2_385
    );
  did_1_MC_Q : X_BUF
    port map (
      I => did_1_MC_Q_tsimrenamed_net_Q_386,
      O => did_1_MC_Q_61
    );
  did_1_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_1_MC_D_387,
      O => did_1_MC_Q_tsimrenamed_net_Q_386
    );
  did_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_1_MC_D_IN0,
      I1 => NlwBufferSignal_did_1_MC_D_IN1,
      O => did_1_MC_D_387
    );
  did_1_MC_D1 : X_ZERO
    port map (
      O => did_1_MC_D1_388
    );
  did_1_MC_D2 : X_ZERO
    port map (
      O => did_1_MC_D2_389
    );
  did_2_MC_Q : X_BUF
    port map (
      I => did_2_MC_Q_tsimrenamed_net_Q_390,
      O => did_2_MC_Q_63
    );
  did_2_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_2_MC_D_391,
      O => did_2_MC_Q_tsimrenamed_net_Q_390
    );
  did_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_2_MC_D_IN0,
      I1 => NlwBufferSignal_did_2_MC_D_IN1,
      O => did_2_MC_D_391
    );
  did_2_MC_D1 : X_ZERO
    port map (
      O => did_2_MC_D1_392
    );
  did_2_MC_D2 : X_ZERO
    port map (
      O => did_2_MC_D2_393
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_65
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_395
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_396
    );
  err_MC_D2_PT_0 : X_AND7
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_0_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_0_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_0_IN6,
      O => err_MC_D2_PT_0_398
    );
  err_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_1_IN5,
      I6 => NlwInverterSignal_err_MC_D2_PT_1_IN6,
      I7 => NlwInverterSignal_err_MC_D2_PT_1_IN7,
      I8 => NlwBufferSignal_err_MC_D2_PT_1_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_1_IN15,
      O => err_MC_D2_PT_1_399
    );
  err_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_2_IN6,
      I7 => NlwBufferSignal_err_MC_D2_PT_2_IN7,
      I8 => NlwInverterSignal_err_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_2_IN15,
      O => err_MC_D2_PT_2_400
    );
  err_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_err_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_err_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_3_IN15,
      O => err_MC_D2_PT_3_401
    );
  err_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_IN3,
      O => err_MC_D2_397
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_D_68,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_71,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_72,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_211_73,
      O => NlwBufferSignal_cnt_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_0_75,
      O => NlwBufferSignal_cnt_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_1_77,
      O => NlwBufferSignal_cnt_0_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_211_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_211_MC_D1_80,
      O => NlwBufferSignal_N_PZ_211_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_211_MC_D2_81,
      O => NlwBufferSignal_N_PZ_211_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_211_MC_D1_IN0 : X_BUF
    port map (
      I => down_cs_82,
      O => NlwBufferSignal_N_PZ_211_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D1_IN1 : X_BUF
    port map (
      I => down_cs_82,
      O => NlwBufferSignal_N_PZ_211_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => up_cs_83,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => carry_v_84,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => up_cs_83,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => carry_v_84,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_211_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_211_MC_D2_PT_0_85,
      O => NlwBufferSignal_N_PZ_211_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_211_MC_D2_PT_1_86,
      O => NlwBufferSignal_N_PZ_211_MC_D2_IN1
    );
  NlwBufferBlock_down_cs_MC_REG_IN : X_BUF
    port map (
      I => down_cs_MC_D_88,
      O => NlwBufferSignal_down_cs_MC_REG_IN
    );
  NlwBufferBlock_down_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_down_cs_MC_REG_CLK
    );
  NlwBufferBlock_down_cs_MC_D_IN0 : X_BUF
    port map (
      I => down_cs_MC_D1_89,
      O => NlwBufferSignal_down_cs_MC_D_IN0
    );
  NlwBufferBlock_down_cs_MC_D_IN1 : X_BUF
    port map (
      I => down_cs_MC_D2_90,
      O => NlwBufferSignal_down_cs_MC_D_IN1
    );
  NlwBufferBlock_down_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_down_cs_MC_D1_IN0
    );
  NlwBufferBlock_down_cs_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_5,
      O => NlwBufferSignal_down_cs_MC_D1_IN1
    );
  NlwBufferBlock_up_cs_MC_REG_IN : X_BUF
    port map (
      I => up_cs_MC_D_92,
      O => NlwBufferSignal_up_cs_MC_REG_IN
    );
  NlwBufferBlock_up_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_up_cs_MC_REG_CLK
    );
  NlwBufferBlock_up_cs_MC_D_IN0 : X_BUF
    port map (
      I => up_cs_MC_D1_93,
      O => NlwBufferSignal_up_cs_MC_D_IN0
    );
  NlwBufferBlock_up_cs_MC_D_IN1 : X_BUF
    port map (
      I => up_cs_MC_D2_94,
      O => NlwBufferSignal_up_cs_MC_D_IN1
    );
  NlwBufferBlock_up_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_up_cs_MC_D1_IN0
    );
  NlwBufferBlock_up_cs_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_7,
      O => NlwBufferSignal_up_cs_MC_D1_IN1
    );
  NlwBufferBlock_carry_v_MC_D_IN0 : X_BUF
    port map (
      I => carry_v_MC_D1_97,
      O => NlwBufferSignal_carry_v_MC_D_IN0
    );
  NlwBufferBlock_carry_v_MC_D_IN1 : X_BUF
    port map (
      I => carry_v_MC_D2_98,
      O => NlwBufferSignal_carry_v_MC_D_IN1
    );
  NlwBufferBlock_carry_v_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_carry_v_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_carry_v_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_carry_v_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_10_MC_UIM_101,
      O => NlwBufferSignal_carry_v_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_carry_v_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_carry_v_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_carry_v_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_carry_v_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_carry_v_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_carry_v_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_carry_v_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_carry_v_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_carry_v_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_11_MC_UIM_106,
      O => NlwBufferSignal_carry_v_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_carry_v_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_carry_v_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_carry_v_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_carry_v_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_carry_v_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_carry_v_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_carry_v_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_carry_v_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_carry_v_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_carry_v_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_carry_v_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_carry_v_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_carry_v_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_carry_v_MC_D2_IN0 : X_BUF
    port map (
      I => carry_v_MC_D2_PT_0_100,
      O => NlwBufferSignal_carry_v_MC_D2_IN0
    );
  NlwBufferBlock_carry_v_MC_D2_IN1 : X_BUF
    port map (
      I => carry_v_MC_D2_PT_1_107,
      O => NlwBufferSignal_carry_v_MC_D2_IN1
    );
  NlwBufferBlock_carry_v_MC_D2_IN2 : X_BUF
    port map (
      I => carry_v_MC_D2_PT_2_113,
      O => NlwBufferSignal_carry_v_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_MC_D_115,
      O => NlwBufferSignal_cnt_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_116,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_117,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_10_MC_UIM_101,
      O => NlwBufferSignal_cnt_10_MC_D1_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_6_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_8_IN5 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_8_IN6 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_8_IN7 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN7
    );
  NlwBufferBlock_cnt_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_0_118,
      O => NlwBufferSignal_cnt_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_1_120,
      O => NlwBufferSignal_cnt_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_2_121,
      O => NlwBufferSignal_cnt_10_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_3_122,
      O => NlwBufferSignal_cnt_10_MC_D2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_4_123,
      O => NlwBufferSignal_cnt_10_MC_D2_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_5_124,
      O => NlwBufferSignal_cnt_10_MC_D2_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_6_125,
      O => NlwBufferSignal_cnt_10_MC_D2_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_7_126,
      O => NlwBufferSignal_cnt_10_MC_D2_IN7
    );
  NlwBufferBlock_cnt_10_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_8_127,
      O => NlwBufferSignal_cnt_10_MC_D2_IN8
    );
  NlwBufferBlock_cnt_10_MC_D2_IN9 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_MC_D2_IN9
    );
  NlwBufferBlock_cnt_10_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_MC_D2_IN10
    );
  NlwBufferBlock_cnt_10_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_MC_D2_IN11
    );
  NlwBufferBlock_cnt_10_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_MC_D2_IN12
    );
  NlwBufferBlock_cnt_10_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_MC_D2_IN13
    );
  NlwBufferBlock_cnt_10_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_MC_D2_IN14
    );
  NlwBufferBlock_cnt_10_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_MC_D2_IN15
    );
  NlwBufferBlock_nLd_cs_MC_REG_IN : X_BUF
    port map (
      I => nLd_cs_MC_D_129,
      O => NlwBufferSignal_nLd_cs_MC_REG_IN
    );
  NlwBufferBlock_nLd_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_nLd_cs_MC_REG_CLK
    );
  NlwBufferBlock_nLd_cs_MC_D_IN0 : X_BUF
    port map (
      I => nLd_cs_MC_D1_130,
      O => NlwBufferSignal_nLd_cs_MC_D_IN0
    );
  NlwBufferBlock_nLd_cs_MC_D_IN1 : X_BUF
    port map (
      I => nLd_cs_MC_D2_131,
      O => NlwBufferSignal_nLd_cs_MC_D_IN1
    );
  NlwBufferBlock_nLd_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_nLd_cs_MC_D1_IN0
    );
  NlwBufferBlock_nLd_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_nLd_cs_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_10_MC_D_133,
      O => NlwBufferSignal_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_10_MC_D1_134,
      O => NlwBufferSignal_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_10_MC_D2_135,
      O => NlwBufferSignal_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_10_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_10_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_10_II_UIM_11,
      O => NlwBufferSignal_valLd_cs_10_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_8_MC_D_137,
      O => NlwBufferSignal_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_8_MC_D1_138,
      O => NlwBufferSignal_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_8_MC_D2_139,
      O => NlwBufferSignal_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_8_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_8_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_8_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_8_II_UIM_13,
      O => NlwBufferSignal_valLd_cs_8_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_9_MC_D_141,
      O => NlwBufferSignal_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_9_MC_D1_142,
      O => NlwBufferSignal_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_9_MC_D2_143,
      O => NlwBufferSignal_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_9_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_9_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_9_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_9_II_UIM_15,
      O => NlwBufferSignal_valLd_cs_9_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_123_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_123_MC_D1_146,
      O => NlwBufferSignal_N_PZ_123_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_123_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_123_MC_D2_147,
      O => NlwBufferSignal_N_PZ_123_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_123_MC_D1_IN0 : X_BUF
    port map (
      I => down_cs_82,
      O => NlwBufferSignal_N_PZ_123_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_123_MC_D1_IN1 : X_BUF
    port map (
      I => up_cs_83,
      O => NlwBufferSignal_N_PZ_123_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_MC_D_149,
      O => NlwBufferSignal_cnt_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_150,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_151,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D1_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_cnt_9_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_8_IN5 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_9_IN5 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_9_IN6 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_0_152,
      O => NlwBufferSignal_cnt_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_1_153,
      O => NlwBufferSignal_cnt_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_2_154,
      O => NlwBufferSignal_cnt_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_3_155,
      O => NlwBufferSignal_cnt_9_MC_D2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_4_156,
      O => NlwBufferSignal_cnt_9_MC_D2_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_5_157,
      O => NlwBufferSignal_cnt_9_MC_D2_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_6_158,
      O => NlwBufferSignal_cnt_9_MC_D2_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_7_159,
      O => NlwBufferSignal_cnt_9_MC_D2_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_8_160,
      O => NlwBufferSignal_cnt_9_MC_D2_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_IN9 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_9_161,
      O => NlwBufferSignal_cnt_9_MC_D2_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_MC_D2_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_MC_D2_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_MC_D2_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_MC_D2_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_MC_D2_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_MC_D2_IN15
    );
  NlwBufferBlock_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_7_MC_D_163,
      O => NlwBufferSignal_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_7_MC_D1_164,
      O => NlwBufferSignal_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_7_MC_D2_165,
      O => NlwBufferSignal_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_7_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_7_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_7_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_7_II_UIM_17,
      O => NlwBufferSignal_valLd_cs_7_MC_D1_IN1
    );
  NlwBufferBlock_carry_v_or0009_MC_D_IN0 : X_BUF
    port map (
      I => carry_v_or0009_MC_D1_168,
      O => NlwBufferSignal_carry_v_or0009_MC_D_IN0
    );
  NlwBufferBlock_carry_v_or0009_MC_D_IN1 : X_BUF
    port map (
      I => carry_v_or0009_MC_D2_169,
      O => NlwBufferSignal_carry_v_or0009_MC_D_IN1
    );
  NlwBufferBlock_carry_v_or0009_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0009_MC_D1_IN0
    );
  NlwBufferBlock_carry_v_or0009_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0009_MC_D1_IN1
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_6_MC_UIM_172,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_6_MC_UIM_172,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_IN0 : X_BUF
    port map (
      I => carry_v_or0009_MC_D2_PT_0_175,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_IN0
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_IN1 : X_BUF
    port map (
      I => carry_v_or0009_MC_D2_PT_1_176,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_IN1
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_IN2 : X_BUF
    port map (
      I => carry_v_or0009_MC_D2_PT_2_181,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_IN2
    );
  NlwBufferBlock_carry_v_or0009_MC_D2_IN3 : X_BUF
    port map (
      I => carry_v_or0009_MC_D2_PT_3_182,
      O => NlwBufferSignal_carry_v_or0009_MC_D2_IN3
    );
  NlwBufferBlock_cnt_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_MC_D_184,
      O => NlwBufferSignal_cnt_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_185,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_186,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_0_188,
      O => NlwBufferSignal_cnt_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_1_189,
      O => NlwBufferSignal_cnt_3_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_128_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_128_MC_D1_192,
      O => NlwBufferSignal_N_PZ_128_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_128_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_128_MC_D2_193,
      O => NlwBufferSignal_N_PZ_128_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_128_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_N_PZ_128_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_128_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_N_PZ_128_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_128_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_N_PZ_128_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_128_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_N_PZ_128_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_128_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_128_MC_D2_PT_0_194,
      O => NlwBufferSignal_N_PZ_128_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_128_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_128_MC_D2_PT_1_195,
      O => NlwBufferSignal_N_PZ_128_MC_D2_IN1
    );
  NlwBufferBlock_carry_v_or0007_MC_D_IN0 : X_BUF
    port map (
      I => carry_v_or0007_MC_D1_198,
      O => NlwBufferSignal_carry_v_or0007_MC_D_IN0
    );
  NlwBufferBlock_carry_v_or0007_MC_D_IN1 : X_BUF
    port map (
      I => carry_v_or0007_MC_D2_199,
      O => NlwBufferSignal_carry_v_or0007_MC_D_IN1
    );
  NlwBufferBlock_carry_v_or0007_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0007_MC_D1_IN0
    );
  NlwBufferBlock_carry_v_or0007_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0007_MC_D1_IN1
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_200,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_198_201,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_2_MC_UIM_202,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_200,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_202,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => N_PZ_209_204,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_199_207,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_210_210,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_IN0 : X_BUF
    port map (
      I => carry_v_or0007_MC_D2_PT_0_203,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_IN0
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_IN1 : X_BUF
    port map (
      I => carry_v_or0007_MC_D2_PT_1_205,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_IN1
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_IN2 : X_BUF
    port map (
      I => carry_v_or0007_MC_D2_PT_2_209,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_IN2
    );
  NlwBufferBlock_carry_v_or0007_MC_D2_IN3 : X_BUF
    port map (
      I => carry_v_or0007_MC_D2_PT_3_211,
      O => NlwBufferSignal_carry_v_or0007_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_D_213,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_214,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_215,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_200,
      O => NlwBufferSignal_cnt_1_MC_D1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_198_201,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_209_204,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_199_207,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_199_207,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_210_210,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => N_PZ_210_210,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_216,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_217,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_2_218,
      O => NlwBufferSignal_cnt_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_3_219,
      O => NlwBufferSignal_cnt_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_4_220,
      O => NlwBufferSignal_cnt_1_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_5_221,
      O => NlwBufferSignal_cnt_1_MC_D2_IN5
    );
  NlwBufferBlock_N_PZ_198_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_198_MC_D1_224,
      O => NlwBufferSignal_N_PZ_198_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_198_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_198_MC_D2_225,
      O => NlwBufferSignal_N_PZ_198_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_198_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_N_PZ_198_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_198_MC_D1_IN1 : X_BUF
    port map (
      I => carry_v_84,
      O => NlwBufferSignal_N_PZ_198_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_1_MC_D_227,
      O => NlwBufferSignal_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_1_MC_D1_228,
      O => NlwBufferSignal_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_1_MC_D2_229,
      O => NlwBufferSignal_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_1_II_UIM_19,
      O => NlwBufferSignal_valLd_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_199_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_199_MC_D1_232,
      O => NlwBufferSignal_N_PZ_199_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_199_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_199_MC_D2_233,
      O => NlwBufferSignal_N_PZ_199_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_199_MC_D1_IN0 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_N_PZ_199_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_199_MC_D1_IN1 : X_BUF
    port map (
      I => carry_v_84,
      O => NlwBufferSignal_N_PZ_199_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_0_MC_D_235,
      O => NlwBufferSignal_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_0_MC_D1_236,
      O => NlwBufferSignal_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_0_MC_D2_237,
      O => NlwBufferSignal_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_0_II_UIM_21,
      O => NlwBufferSignal_valLd_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_209_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_209_MC_D1_240,
      O => NlwBufferSignal_N_PZ_209_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_209_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_209_MC_D2_241,
      O => NlwBufferSignal_N_PZ_209_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_209_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_N_PZ_209_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_209_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => carry_v_84,
      O => NlwBufferSignal_N_PZ_209_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_209_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => down_cs_82,
      O => NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_209_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => up_cs_83,
      O => NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_209_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_198_201,
      O => NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_209_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => down_cs_82,
      O => NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_209_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => up_cs_83,
      O => NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_209_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_198_201,
      O => NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_209_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_209_MC_D2_PT_0_242,
      O => NlwBufferSignal_N_PZ_209_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_209_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_209_MC_D2_PT_1_243,
      O => NlwBufferSignal_N_PZ_209_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_209_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_209_MC_D2_PT_2_244,
      O => NlwBufferSignal_N_PZ_209_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_210_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_210_MC_D1_247,
      O => NlwBufferSignal_N_PZ_210_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_210_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_210_MC_D2_248,
      O => NlwBufferSignal_N_PZ_210_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_210_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_N_PZ_210_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_210_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => carry_v_84,
      O => NlwBufferSignal_N_PZ_210_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_210_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => down_cs_82,
      O => NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_210_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => up_cs_83,
      O => NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_210_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_199_207,
      O => NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_210_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => down_cs_82,
      O => NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_210_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => up_cs_83,
      O => NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_210_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_199_207,
      O => NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_210_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_210_MC_D2_PT_0_249,
      O => NlwBufferSignal_N_PZ_210_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_210_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_210_MC_D2_PT_1_250,
      O => NlwBufferSignal_N_PZ_210_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_210_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_210_MC_D2_PT_2_251,
      O => NlwBufferSignal_N_PZ_210_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_D_253,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_254,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_255,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_202,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_198_201,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_2_MC_UIM_202,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_MC_UIM_200,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_209_204,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_200,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => N_PZ_198_201,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_2_MC_UIM_202,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => N_PZ_199_207,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => N_PZ_210_210,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => N_PZ_199_207,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_256,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_257,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_258,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_3_259,
      O => NlwBufferSignal_cnt_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_4_260,
      O => NlwBufferSignal_cnt_2_MC_D2_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_5_261,
      O => NlwBufferSignal_cnt_2_MC_D2_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_6_262,
      O => NlwBufferSignal_cnt_2_MC_D2_IN6
    );
  NlwBufferBlock_cnt_2_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_7_263,
      O => NlwBufferSignal_cnt_2_MC_D2_IN7
    );
  NlwBufferBlock_cnt_2_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_8_264,
      O => NlwBufferSignal_cnt_2_MC_D2_IN8
    );
  NlwBufferBlock_cnt_2_MC_D2_IN9 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_2_MC_D2_IN9
    );
  NlwBufferBlock_cnt_2_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_2_MC_D2_IN10
    );
  NlwBufferBlock_cnt_2_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_2_MC_D2_IN11
    );
  NlwBufferBlock_cnt_2_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_2_MC_D2_IN12
    );
  NlwBufferBlock_cnt_2_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_2_MC_D2_IN13
    );
  NlwBufferBlock_cnt_2_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_2_MC_D2_IN14
    );
  NlwBufferBlock_cnt_2_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_2_MC_D2_IN15
    );
  NlwBufferBlock_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_2_MC_D_266,
      O => NlwBufferSignal_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_2_MC_D1_267,
      O => NlwBufferSignal_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_2_MC_D2_268,
      O => NlwBufferSignal_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_2_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_2_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_2_II_UIM_23,
      O => NlwBufferSignal_valLd_cs_2_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_3_MC_D_270,
      O => NlwBufferSignal_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_3_MC_D1_271,
      O => NlwBufferSignal_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_3_MC_D2_272,
      O => NlwBufferSignal_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_3_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_3_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_3_II_UIM_25,
      O => NlwBufferSignal_valLd_cs_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_MC_D_274,
      O => NlwBufferSignal_cnt_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_275,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_276,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_6_MC_UIM_172,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_6_MC_UIM_172,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_6_MC_UIM_172,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN6 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN7 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN7 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_8_IN5 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_8_IN6 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_8_IN7 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_9_IN5 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_9_IN6 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_9_IN7 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_0_277,
      O => NlwBufferSignal_cnt_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_1_278,
      O => NlwBufferSignal_cnt_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_2_279,
      O => NlwBufferSignal_cnt_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_3_280,
      O => NlwBufferSignal_cnt_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_4_281,
      O => NlwBufferSignal_cnt_6_MC_D2_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_5_282,
      O => NlwBufferSignal_cnt_6_MC_D2_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_6_283,
      O => NlwBufferSignal_cnt_6_MC_D2_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_7_284,
      O => NlwBufferSignal_cnt_6_MC_D2_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_8_285,
      O => NlwBufferSignal_cnt_6_MC_D2_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_IN9 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_9_286,
      O => NlwBufferSignal_cnt_6_MC_D2_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_6_MC_D2_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_6_MC_D2_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_6_MC_D2_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_6_MC_D2_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_6_MC_D2_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_6_MC_D2_IN15
    );
  NlwBufferBlock_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_6_MC_D_288,
      O => NlwBufferSignal_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_6_MC_D1_289,
      O => NlwBufferSignal_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_6_MC_D2_290,
      O => NlwBufferSignal_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_6_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_6_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_6_II_UIM_27,
      O => NlwBufferSignal_valLd_cs_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D_292,
      O => NlwBufferSignal_cnt_5_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D1_293,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_294,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D1_IN2 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D1_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_5_IN6 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN6
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_6_IN6 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN6
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_8_IN5 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN5
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_8_IN6 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN6
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_9_IN5 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN5
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_9_IN6 : X_BUF
    port map (
      I => N_PZ_128_187,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN6
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_0_295,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_1_296,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_2_297,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_3_298,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_4_299,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN4
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_5_300,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN5
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_6_301,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN6
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_7_302,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN7
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_8_303,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN8
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN9 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_9_304,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN9
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN10
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN11
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN12
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN13
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN14
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN15
    );
  NlwBufferBlock_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_5_MC_D_306,
      O => NlwBufferSignal_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_5_MC_D1_307,
      O => NlwBufferSignal_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_5_MC_D2_308,
      O => NlwBufferSignal_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_5_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_5_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_5_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_5_II_UIM_29,
      O => NlwBufferSignal_valLd_cs_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D_310,
      O => NlwBufferSignal_cnt_4_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D1_311,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_312,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D1_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_171,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => carry_v_or0007_170,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_0_313,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_1_314,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_2_315,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_3_316,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_4_317,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN4
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_5_318,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN5
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_6_319,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN6
    );
  NlwBufferBlock_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_4_MC_D_321,
      O => NlwBufferSignal_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_4_MC_D1_322,
      O => NlwBufferSignal_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_4_MC_D2_323,
      O => NlwBufferSignal_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_4_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_4_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_4_II_UIM_31,
      O => NlwBufferSignal_valLd_cs_4_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D_325,
      O => NlwBufferSignal_cnt_7_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D1_326,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_327,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_0_328,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_1_329,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_2_330,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_3_331,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_4_332,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN4
    );
  NlwBufferBlock_N_PZ_132_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_132_MC_D1_335,
      O => NlwBufferSignal_N_PZ_132_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_132_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_132_MC_D2_336,
      O => NlwBufferSignal_N_PZ_132_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_132_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_N_PZ_132_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_132_MC_D1_IN1 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_N_PZ_132_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_132_MC_D1_IN2 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_N_PZ_132_MC_D1_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D_338,
      O => NlwBufferSignal_cnt_8_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D1_339,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_340,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_0_341,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_1_342,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_2_343,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_3_344,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_4_345,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_5_346,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN5
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_6_347,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN6
    );
  NlwBufferBlock_cnt_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_MC_D_349,
      O => NlwBufferSignal_cnt_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_350,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_351,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_11_MC_UIM_106,
      O => NlwBufferSignal_cnt_11_MC_D1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN6 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => cnt_10_MC_UIM_101,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN7 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => cnt_10_MC_UIM_101,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN5 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN6 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN7 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN5 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN6 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN7 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN8 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN15
    );
  NlwBufferBlock_cnt_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_0_352,
      O => NlwBufferSignal_cnt_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_1_353,
      O => NlwBufferSignal_cnt_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_2_354,
      O => NlwBufferSignal_cnt_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_3_355,
      O => NlwBufferSignal_cnt_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_4_356,
      O => NlwBufferSignal_cnt_11_MC_D2_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_5_357,
      O => NlwBufferSignal_cnt_11_MC_D2_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_6_358,
      O => NlwBufferSignal_cnt_11_MC_D2_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_7_359,
      O => NlwBufferSignal_cnt_11_MC_D2_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_8_360,
      O => NlwBufferSignal_cnt_11_MC_D2_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_IN9 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_9_361,
      O => NlwBufferSignal_cnt_11_MC_D2_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_11_MC_D2_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_11_MC_D2_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_11_MC_D2_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_11_MC_D2_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_11_MC_D2_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_11_MC_D2_IN15
    );
  NlwBufferBlock_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_11_MC_D_363,
      O => NlwBufferSignal_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_11_MC_D1_364,
      O => NlwBufferSignal_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_11_MC_D2_365,
      O => NlwBufferSignal_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_11_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_11_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_11_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_valLd_cs_11_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_368,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_369,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_4_MC_D1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_4_BUFR_174,
      O => NlwBufferSignal_cnt_4_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_372,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_373,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_5_MC_D1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_5_BUFR_173,
      O => NlwBufferSignal_cnt_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_376,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_377,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_7_MC_D1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_cnt_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_380,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_381,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_8_MC_D1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_cnt_8_MC_D1_IN1
    );
  NlwBufferBlock_did_0_MC_REG_IN : X_BUF
    port map (
      I => did_0_MC_D_383,
      O => NlwBufferSignal_did_0_MC_REG_IN
    );
  NlwBufferBlock_did_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_did_0_MC_REG_CLK
    );
  NlwBufferBlock_did_0_MC_D_IN0 : X_BUF
    port map (
      I => did_0_MC_D1_384,
      O => NlwBufferSignal_did_0_MC_D_IN0
    );
  NlwBufferBlock_did_0_MC_D_IN1 : X_BUF
    port map (
      I => did_0_MC_D2_385,
      O => NlwBufferSignal_did_0_MC_D_IN1
    );
  NlwBufferBlock_did_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_did_0_MC_D1_IN0
    );
  NlwBufferBlock_did_0_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_did_0_MC_D1_IN1
    );
  NlwBufferBlock_did_1_MC_D_IN0 : X_BUF
    port map (
      I => did_1_MC_D1_388,
      O => NlwBufferSignal_did_1_MC_D_IN0
    );
  NlwBufferBlock_did_1_MC_D_IN1 : X_BUF
    port map (
      I => did_1_MC_D2_389,
      O => NlwBufferSignal_did_1_MC_D_IN1
    );
  NlwBufferBlock_did_2_MC_D_IN0 : X_BUF
    port map (
      I => did_2_MC_D1_392,
      O => NlwBufferSignal_did_2_MC_D_IN0
    );
  NlwBufferBlock_did_2_MC_D_IN1 : X_BUF
    port map (
      I => did_2_MC_D2_393,
      O => NlwBufferSignal_did_2_MC_D_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_395,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_396,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_397,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_err_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_err_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_err_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_err_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => N_PZ_132_119,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_10_MC_UIM_101,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_11_MC_UIM_106,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_10_MC_UIM_101,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_9_MC_UIM_102,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_7_BUFR_104,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_8_BUFR_105,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_11_MC_UIM_106,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_74,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_123_99,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => carry_v_or0009_103,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => err_MC_D2_PT_0_398,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => err_MC_D2_PT_1_399,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_D2_IN2 : X_BUF
    port map (
      I => err_MC_D2_PT_2_400,
      O => NlwBufferSignal_err_MC_D2_IN2
    );
  NlwBufferBlock_err_MC_D2_IN3 : X_BUF
    port map (
      I => err_MC_D2_PT_3_401,
      O => NlwBufferSignal_err_MC_D2_IN3
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_211_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_211_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_carry_v_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D_IN0,
      O => NlwInverterSignal_carry_v_MC_D_IN0
    );
  NlwInverterBlock_carry_v_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_carry_v_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_carry_v_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_carry_v_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_carry_v_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_carry_v_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_carry_v_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_carry_v_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_carry_v_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_carry_v_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_carry_v_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_carry_v_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_carry_v_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_carry_v_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_carry_v_MC_D2_PT_1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_1_IN6,
      O => NlwInverterSignal_carry_v_MC_D2_PT_1_IN6
    );
  NlwInverterBlock_carry_v_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_carry_v_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_carry_v_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_carry_v_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_carry_v_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_carry_v_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_carry_v_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_carry_v_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_carry_v_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_carry_v_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_carry_v_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_carry_v_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_carry_v_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_carry_v_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_6_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_6_IN6,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_6_IN6
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_8_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_8_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_8_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_8_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_8_IN4
    );
  NlwInverterBlock_N_PZ_123_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_123_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_123_MC_D1_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_7_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN4,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_7_IN4
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_8_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_8_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_8_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_9_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_9_IN5,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_9_IN5
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_carry_v_or0009_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0009_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_carry_v_or0009_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_128_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_128_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_128_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_N_PZ_128_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_128_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_128_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_carry_v_or0007_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_carry_v_or0007_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_carry_v_or0007_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0007_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_carry_v_or0007_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_carry_v_or0007_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0007_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_carry_v_or0007_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_carry_v_or0007_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_carry_v_or0007_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_carry_v_or0007_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0007_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_carry_v_or0007_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_carry_v_or0007_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_carry_v_or0007_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_carry_v_or0007_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_v_or0007_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_carry_v_or0007_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_N_PZ_198_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_198_MC_D1_IN0,
      O => NlwInverterSignal_N_PZ_198_MC_D1_IN0
    );
  NlwInverterBlock_N_PZ_198_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_198_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_198_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_199_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_199_MC_D1_IN0,
      O => NlwInverterSignal_N_PZ_199_MC_D1_IN0
    );
  NlwInverterBlock_N_PZ_199_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_199_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_199_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_209_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_209_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_209_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_209_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_209_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_209_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_N_PZ_209_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_N_PZ_209_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_209_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_N_PZ_209_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_N_PZ_210_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_210_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_210_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_210_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_210_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_210_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_N_PZ_210_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_N_PZ_210_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_210_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_N_PZ_210_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN7
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN7
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_8_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_8_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_8_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN6
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_8_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_8_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_8_IN7
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_9_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_9_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_9_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_9_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_9_IN7
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D1_IN1,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D1_IN1
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_5_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_5_IN5,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_5_IN5
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_6_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_6_IN6,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_6_IN6
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_8_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_8_IN6,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_8_IN6
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_9_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN3,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN3
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_9_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN5,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN5
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_9_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_9_IN6,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_9_IN6
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_5_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_5_IN5,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_5_IN5
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_N_PZ_132_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_132_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_132_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_132_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_132_MC_D1_IN2,
      O => NlwInverterSignal_N_PZ_132_MC_D1_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_7_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_7_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN6,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN6
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_7_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN7,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN7
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_8_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_8_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_9_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN8,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN8
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN6,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN6
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN7 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN7,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN7
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN8 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN8,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN8
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN8
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure;

