--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: counter_timesim.vhd
-- /___/   /\     Timestamp: Fri Dec 05 13:42:08 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm counter -w -dir netgen/fit -ofmt vhdl -sim counter.nga counter_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: counter.nga
-- Output file	: D:\DT\ISE_a03\netgen\fit\counter_timesim.vhd
-- # of Entities	: 1
-- Design Name	: counter.nga
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity counter is
  port (
    clk : in STD_LOGIC := 'X'; 
    nres : in STD_LOGIC := 'X'; 
    nLd : in STD_LOGIC := 'X'; 
    up : in STD_LOGIC := 'X'; 
    down : in STD_LOGIC := 'X'; 
    err : out STD_LOGIC; 
    valLd : in STD_LOGIC_VECTOR ( 11 downto 0 ); 
    cnt : out STD_LOGIC_VECTOR ( 11 downto 0 ); 
    did : out STD_LOGIC_VECTOR ( 2 downto 0 ) 
  );
end counter;

architecture Structure of counter is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal nLd_II_UIM_5 : STD_LOGIC; 
  signal valLd_0_II_UIM_7 : STD_LOGIC; 
  signal up_II_UIM_9 : STD_LOGIC; 
  signal down_II_UIM_11 : STD_LOGIC; 
  signal valLd_7_II_UIM_13 : STD_LOGIC; 
  signal valLd_8_II_UIM_15 : STD_LOGIC; 
  signal valLd_1_II_UIM_17 : STD_LOGIC; 
  signal valLd_2_II_UIM_19 : STD_LOGIC; 
  signal valLd_3_II_UIM_21 : STD_LOGIC; 
  signal valLd_4_II_UIM_23 : STD_LOGIC; 
  signal valLd_5_II_UIM_25 : STD_LOGIC; 
  signal valLd_6_II_UIM_27 : STD_LOGIC; 
  signal valLd_9_II_UIM_29 : STD_LOGIC; 
  signal valLd_10_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal cnt_0_MC_Q_35 : STD_LOGIC; 
  signal cnt_10_MC_Q_37 : STD_LOGIC; 
  signal cnt_11_MC_Q_39 : STD_LOGIC; 
  signal cnt_1_MC_Q_41 : STD_LOGIC; 
  signal cnt_2_MC_Q_43 : STD_LOGIC; 
  signal cnt_3_MC_Q_45 : STD_LOGIC; 
  signal cnt_4_MC_Q_47 : STD_LOGIC; 
  signal cnt_5_MC_Q_49 : STD_LOGIC; 
  signal cnt_6_MC_Q_51 : STD_LOGIC; 
  signal cnt_7_MC_Q_53 : STD_LOGIC; 
  signal cnt_8_MC_Q_55 : STD_LOGIC; 
  signal cnt_9_MC_Q_57 : STD_LOGIC; 
  signal did_0_MC_Q_59 : STD_LOGIC; 
  signal did_1_MC_Q_61 : STD_LOGIC; 
  signal did_2_MC_Q_63 : STD_LOGIC; 
  signal err_MC_Q_65 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_UIM_67 : STD_LOGIC; 
  signal cnt_0_MC_D_68 : STD_LOGIC; 
  signal Gnd_69 : STD_LOGIC; 
  signal Vcc_70 : STD_LOGIC; 
  signal cnt_0_MC_D1_71 : STD_LOGIC; 
  signal cnt_0_MC_D2_72 : STD_LOGIC; 
  signal nLd_cs_73 : STD_LOGIC; 
  signal N_PZ_199_75 : STD_LOGIC; 
  signal N_PZ_230_76 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_0_77 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_1_78 : STD_LOGIC; 
  signal up_cs_79 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_2_80 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_3_81 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_4_82 : STD_LOGIC; 
  signal nLd_cs_MC_Q : STD_LOGIC; 
  signal nLd_cs_MC_D_84 : STD_LOGIC; 
  signal nLd_cs_MC_D1_85 : STD_LOGIC; 
  signal nLd_cs_MC_D2_86 : STD_LOGIC; 
  signal valLd_cs_0_MC_Q : STD_LOGIC; 
  signal valLd_cs_0_MC_D_88 : STD_LOGIC; 
  signal valLd_cs_0_MC_D1_89 : STD_LOGIC; 
  signal valLd_cs_0_MC_D2_90 : STD_LOGIC; 
  signal up_cs_MC_Q : STD_LOGIC; 
  signal up_cs_MC_D_92 : STD_LOGIC; 
  signal up_cs_MC_D1_93 : STD_LOGIC; 
  signal up_cs_MC_D2_94 : STD_LOGIC; 
  signal N_PZ_199_MC_Q_95 : STD_LOGIC; 
  signal N_PZ_199_MC_D_96 : STD_LOGIC; 
  signal N_PZ_199_MC_D1_97 : STD_LOGIC; 
  signal N_PZ_199_MC_D2_98 : STD_LOGIC; 
  signal down_cs_99 : STD_LOGIC; 
  signal down_cs_MC_Q : STD_LOGIC; 
  signal down_cs_MC_D_101 : STD_LOGIC; 
  signal down_cs_MC_D1_102 : STD_LOGIC; 
  signal down_cs_MC_D2_103 : STD_LOGIC; 
  signal N_PZ_230_MC_Q_104 : STD_LOGIC; 
  signal N_PZ_230_MC_D_105 : STD_LOGIC; 
  signal N_PZ_230_MC_D1_106 : STD_LOGIC; 
  signal N_PZ_230_MC_D2_107 : STD_LOGIC; 
  signal N_PZ_230_MC_D2_PT_0_108 : STD_LOGIC; 
  signal N_PZ_230_MC_D2_PT_1_109 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q_110 : STD_LOGIC; 
  signal cnt_10_MC_D_111 : STD_LOGIC; 
  signal cnt_10_MC_D1_112 : STD_LOGIC; 
  signal cnt_10_MC_D2_113 : STD_LOGIC; 
  signal cnt_10_BUFR_114 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D_116 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D1_117 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_118 : STD_LOGIC; 
  signal N_PZ_167_119 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_0_121 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_1_122 : STD_LOGIC; 
  signal cnt_9_BUFR_123 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_2_124 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_3_126 : STD_LOGIC; 
  signal cnt_8_BUFR_127 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_4_128 : STD_LOGIC; 
  signal cnt_7_BUFR_129 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_5_130 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_6_131 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_7_132 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_8_133 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D_135 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D1_136 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_137 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_0_138 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_1_139 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_2_142 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_3_143 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_4_144 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_5_145 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_6_146 : STD_LOGIC; 
  signal valLd_cs_7_MC_Q : STD_LOGIC; 
  signal valLd_cs_7_MC_D_148 : STD_LOGIC; 
  signal valLd_cs_7_MC_D1_149 : STD_LOGIC; 
  signal valLd_cs_7_MC_D2_150 : STD_LOGIC; 
  signal valLd_cs_8_MC_Q : STD_LOGIC; 
  signal valLd_cs_8_MC_D_152 : STD_LOGIC; 
  signal valLd_cs_8_MC_D1_153 : STD_LOGIC; 
  signal valLd_cs_8_MC_D2_154 : STD_LOGIC; 
  signal carry_s_7_MC_Q_155 : STD_LOGIC; 
  signal carry_s_7_MC_D_156 : STD_LOGIC; 
  signal carry_s_7_MC_D1_157 : STD_LOGIC; 
  signal carry_s_7_MC_D2_158 : STD_LOGIC; 
  signal cnt_4_MC_UIM_159 : STD_LOGIC; 
  signal cnt_3_BUFR_160 : STD_LOGIC; 
  signal N_PZ_211_161 : STD_LOGIC; 
  signal cnt_5_MC_UIM_162 : STD_LOGIC; 
  signal cnt_6_MC_UIM_163 : STD_LOGIC; 
  signal carry_s_7_MC_D2_PT_0_164 : STD_LOGIC; 
  signal cnt_2_MC_UIM_165 : STD_LOGIC; 
  signal N_PZ_214_166 : STD_LOGIC; 
  signal carry_s_7_MC_D2_PT_1_167 : STD_LOGIC; 
  signal carry_s_7_MC_D2_PT_2_174 : STD_LOGIC; 
  signal carry_s_7_MC_D2_PT_3_175 : STD_LOGIC; 
  signal valLd_cs_1_MC_Q : STD_LOGIC; 
  signal valLd_cs_1_MC_D_177 : STD_LOGIC; 
  signal valLd_cs_1_MC_D1_178 : STD_LOGIC; 
  signal valLd_cs_1_MC_D2_179 : STD_LOGIC; 
  signal valLd_cs_2_MC_Q : STD_LOGIC; 
  signal valLd_cs_2_MC_D_181 : STD_LOGIC; 
  signal valLd_cs_2_MC_D1_182 : STD_LOGIC; 
  signal valLd_cs_2_MC_D2_183 : STD_LOGIC; 
  signal valLd_cs_3_MC_Q : STD_LOGIC; 
  signal valLd_cs_3_MC_D_185 : STD_LOGIC; 
  signal valLd_cs_3_MC_D1_186 : STD_LOGIC; 
  signal valLd_cs_3_MC_D2_187 : STD_LOGIC; 
  signal valLd_cs_4_MC_Q : STD_LOGIC; 
  signal valLd_cs_4_MC_D_189 : STD_LOGIC; 
  signal valLd_cs_4_MC_D1_190 : STD_LOGIC; 
  signal valLd_cs_4_MC_D2_191 : STD_LOGIC; 
  signal valLd_cs_5_MC_Q : STD_LOGIC; 
  signal valLd_cs_5_MC_D_193 : STD_LOGIC; 
  signal valLd_cs_5_MC_D1_194 : STD_LOGIC; 
  signal valLd_cs_5_MC_D2_195 : STD_LOGIC; 
  signal valLd_cs_6_MC_Q : STD_LOGIC; 
  signal valLd_cs_6_MC_D_197 : STD_LOGIC; 
  signal valLd_cs_6_MC_D1_198 : STD_LOGIC; 
  signal valLd_cs_6_MC_D2_199 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_4_MC_D_201 : STD_LOGIC; 
  signal cnt_4_MC_D1_202 : STD_LOGIC; 
  signal cnt_4_MC_D2_203 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_0_204 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_1_205 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_2_206 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_3_207 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_4_208 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_5_209 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_6_210 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_7_211 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_8_212 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D_214 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D1_215 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_216 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_0_217 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_1_218 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_2_219 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_3_220 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_4_221 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_5_222 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_6_223 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_7_224 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_8_225 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_9_226 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_10_227 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_11_228 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_12_229 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_13_230 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_232 : STD_LOGIC; 
  signal cnt_2_MC_D1_233 : STD_LOGIC; 
  signal cnt_2_MC_D2_234 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_235 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_236 : STD_LOGIC; 
  signal cnt_1_MC_UIM_237 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_238 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_3_239 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_4_240 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_5_241 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_6_242 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_7_243 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_245 : STD_LOGIC; 
  signal cnt_1_MC_D1_246 : STD_LOGIC; 
  signal cnt_1_MC_D2_247 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_248 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_249 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_2_250 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_3_251 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_4_252 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_5_253 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_6_254 : STD_LOGIC; 
  signal N_PZ_211_MC_Q_255 : STD_LOGIC; 
  signal N_PZ_211_MC_D_256 : STD_LOGIC; 
  signal N_PZ_211_MC_D1_257 : STD_LOGIC; 
  signal N_PZ_211_MC_D2_258 : STD_LOGIC; 
  signal N_PZ_211_MC_D2_PT_0_259 : STD_LOGIC; 
  signal N_PZ_211_MC_D2_PT_1_260 : STD_LOGIC; 
  signal N_PZ_214_MC_Q_261 : STD_LOGIC; 
  signal N_PZ_214_MC_D_262 : STD_LOGIC; 
  signal N_PZ_214_MC_D1_263 : STD_LOGIC; 
  signal N_PZ_214_MC_D2_264 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_5_MC_D_266 : STD_LOGIC; 
  signal cnt_5_MC_D1_267 : STD_LOGIC; 
  signal cnt_5_MC_D2_268 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_0_269 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_1_270 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_2_271 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_3_272 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_4_273 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_5_274 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_6_275 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_7_276 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_8_277 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_9_278 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_10_279 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_6_MC_D_281 : STD_LOGIC; 
  signal cnt_6_MC_D1_282 : STD_LOGIC; 
  signal cnt_6_MC_D2_283 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_0_284 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_1_285 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_2_286 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_3_287 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_4_288 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_5_289 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_6_290 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_7_291 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D_293 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D1_294 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_295 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_0_296 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_1_297 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_2_298 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_PT_3_299 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D_301 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D1_302 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_303 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_0_304 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_1_306 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_2_307 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_3_308 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_4_309 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_5_310 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_6_311 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_7_312 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_8_313 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_9_314 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_10_315 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_11_316 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_PT_12_317 : STD_LOGIC; 
  signal valLd_cs_9_MC_Q : STD_LOGIC; 
  signal valLd_cs_9_MC_D_319 : STD_LOGIC; 
  signal valLd_cs_9_MC_D1_320 : STD_LOGIC; 
  signal valLd_cs_9_MC_D2_321 : STD_LOGIC; 
  signal N_PZ_167_MC_Q_322 : STD_LOGIC; 
  signal N_PZ_167_MC_D_323 : STD_LOGIC; 
  signal N_PZ_167_MC_D1_324 : STD_LOGIC; 
  signal N_PZ_167_MC_D2_325 : STD_LOGIC; 
  signal N_PZ_167_MC_D2_PT_0_326 : STD_LOGIC; 
  signal N_PZ_167_MC_D2_PT_1_327 : STD_LOGIC; 
  signal N_PZ_167_MC_D2_PT_2_328 : STD_LOGIC; 
  signal N_PZ_167_MC_D2_PT_3_329 : STD_LOGIC; 
  signal valLd_cs_10_MC_Q : STD_LOGIC; 
  signal valLd_cs_10_MC_D_331 : STD_LOGIC; 
  signal valLd_cs_10_MC_D1_332 : STD_LOGIC; 
  signal valLd_cs_10_MC_D2_333 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_11_MC_UIM_335 : STD_LOGIC; 
  signal cnt_11_MC_D_336 : STD_LOGIC; 
  signal cnt_11_MC_D1_337 : STD_LOGIC; 
  signal cnt_11_MC_D2_338 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_0_340 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_1_341 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_2_342 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_3_343 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_4_344 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_5_345 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_6_346 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_7_347 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_8_348 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_9_349 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_10_350 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_11_351 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_12_352 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_13_353 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_14_354 : STD_LOGIC; 
  signal valLd_cs_11_MC_Q : STD_LOGIC; 
  signal valLd_cs_11_MC_D_356 : STD_LOGIC; 
  signal valLd_cs_11_MC_D1_357 : STD_LOGIC; 
  signal valLd_cs_11_MC_D2_358 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q_359 : STD_LOGIC; 
  signal cnt_3_MC_D_360 : STD_LOGIC; 
  signal cnt_3_MC_D1_361 : STD_LOGIC; 
  signal cnt_3_MC_D2_362 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q_363 : STD_LOGIC; 
  signal cnt_7_MC_D_364 : STD_LOGIC; 
  signal cnt_7_MC_D1_365 : STD_LOGIC; 
  signal cnt_7_MC_D2_366 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q_367 : STD_LOGIC; 
  signal cnt_8_MC_D_368 : STD_LOGIC; 
  signal cnt_8_MC_D1_369 : STD_LOGIC; 
  signal cnt_8_MC_D2_370 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q_371 : STD_LOGIC; 
  signal cnt_9_MC_D_372 : STD_LOGIC; 
  signal cnt_9_MC_D1_373 : STD_LOGIC; 
  signal cnt_9_MC_D2_374 : STD_LOGIC; 
  signal did_0_MC_Q_tsimrenamed_net_Q_375 : STD_LOGIC; 
  signal did_0_MC_D_376 : STD_LOGIC; 
  signal did_0_MC_D1_377 : STD_LOGIC; 
  signal did_0_MC_D2_378 : STD_LOGIC; 
  signal did_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal did_1_MC_D_380 : STD_LOGIC; 
  signal did_1_MC_D1_381 : STD_LOGIC; 
  signal did_1_MC_D2_382 : STD_LOGIC; 
  signal did_2_MC_Q_tsimrenamed_net_Q_383 : STD_LOGIC; 
  signal did_2_MC_D_384 : STD_LOGIC; 
  signal did_2_MC_D1_385 : STD_LOGIC; 
  signal did_2_MC_D2_386 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_D_388 : STD_LOGIC; 
  signal err_MC_D1_389 : STD_LOGIC; 
  signal err_MC_D2_390 : STD_LOGIC; 
  signal err_MC_D2_PT_0_391 : STD_LOGIC; 
  signal err_MC_D2_PT_1_392 : STD_LOGIC; 
  signal err_MC_D2_PT_2_393 : STD_LOGIC; 
  signal err_MC_D2_PT_3_394 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_up_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_199_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_199_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_199_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_199_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_down_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_230_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_230_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_230_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_230_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_230_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_230_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_230_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_230_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_carry_s_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_6_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_8_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_211_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_214_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_214_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_214_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_214_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_214_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_8_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_9_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_10_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_167_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_10_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_10_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_11_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_11_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_11_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_11_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_11_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_11_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_12_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_12_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_12_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_12_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_12_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_13_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_14_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_199_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_230_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_8_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_8_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_8_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_11_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_11_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_214_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_9_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_9_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_9_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_10_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_10_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_10_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_10_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_12_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_167_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_167_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_167_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_11_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_11_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_12_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_12_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_12_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_13_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_13_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_13_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_13_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_13_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_13_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_14_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_14_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal carry_s : STD_LOGIC_VECTOR ( 7 downto 7 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_5
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_7
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_9
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_11
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_13
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_15
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_17
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_19
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_21
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_23
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_25
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_27
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_29
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_35,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_37,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_39,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_41,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_43,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_45,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_47,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_49,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_51,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_53,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_55,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_57,
      O => cnt(9)
    );
  did_0_Q : X_BUF
    port map (
      I => did_0_MC_Q_59,
      O => did(0)
    );
  did_1_Q : X_BUF
    port map (
      I => did_1_MC_Q_61,
      O => did(1)
    );
  did_2_Q : X_BUF
    port map (
      I => did_2_MC_Q_63,
      O => did(2)
    );
  err_66 : X_BUF
    port map (
      I => err_MC_Q_65,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_35
    );
  cnt_0_MC_UIM : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_UIM_67
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_69
    );
  Vcc : X_ONE
    port map (
      O => Vcc_70
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_68
    );
  cnt_0_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D1_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D1_IN2,
      O => cnt_0_MC_D1_71
    );
  cnt_0_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN3,
      O => cnt_0_MC_D2_PT_0_77
    );
  cnt_0_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3,
      O => cnt_0_MC_D2_PT_1_78
    );
  cnt_0_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN3,
      O => cnt_0_MC_D2_PT_2_80
    );
  cnt_0_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN3,
      O => cnt_0_MC_D2_PT_3_81
    );
  cnt_0_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_0_MC_D2_PT_4_IN4,
      O => cnt_0_MC_D2_PT_4_82
    );
  cnt_0_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_0_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_0_MC_D2_IN4,
      O => cnt_0_MC_D2_72
    );
  nLd_cs : X_BUF
    port map (
      I => nLd_cs_MC_Q,
      O => nLd_cs_73
    );
  nLd_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_nLd_cs_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_nLd_cs_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => nLd_cs_MC_Q
    );
  nLd_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_nLd_cs_MC_D_IN0,
      I1 => NlwBufferSignal_nLd_cs_MC_D_IN1,
      O => nLd_cs_MC_D_84
    );
  nLd_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_nLd_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_nLd_cs_MC_D1_IN1,
      O => nLd_cs_MC_D1_85
    );
  nLd_cs_MC_D2 : X_ZERO
    port map (
      O => nLd_cs_MC_D2_86
    );
  valLd_cs_0_Q : X_BUF
    port map (
      I => valLd_cs_0_MC_Q,
      O => valLd_cs(0)
    );
  valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_0_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_0_MC_Q
    );
  valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_0_MC_D_IN1,
      O => valLd_cs_0_MC_D_88
    );
  valLd_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_0_MC_D1_IN1,
      O => valLd_cs_0_MC_D1_89
    );
  valLd_cs_0_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_0_MC_D2_90
    );
  up_cs : X_BUF
    port map (
      I => up_cs_MC_Q,
      O => up_cs_79
    );
  up_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_up_cs_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_up_cs_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => up_cs_MC_Q
    );
  up_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_up_cs_MC_D_IN0,
      I1 => NlwBufferSignal_up_cs_MC_D_IN1,
      O => up_cs_MC_D_92
    );
  up_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_up_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_up_cs_MC_D1_IN1,
      O => up_cs_MC_D1_93
    );
  up_cs_MC_D2 : X_ZERO
    port map (
      O => up_cs_MC_D2_94
    );
  N_PZ_199 : X_BUF
    port map (
      I => N_PZ_199_MC_Q_95,
      O => N_PZ_199_75
    );
  N_PZ_199_MC_Q : X_BUF
    port map (
      I => N_PZ_199_MC_D_96,
      O => N_PZ_199_MC_Q_95
    );
  N_PZ_199_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_199_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_199_MC_D_IN1,
      O => N_PZ_199_MC_D_96
    );
  N_PZ_199_MC_D1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_199_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_199_MC_D1_IN1,
      O => N_PZ_199_MC_D1_97
    );
  N_PZ_199_MC_D2 : X_ZERO
    port map (
      O => N_PZ_199_MC_D2_98
    );
  down_cs : X_BUF
    port map (
      I => down_cs_MC_Q,
      O => down_cs_99
    );
  down_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_down_cs_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_down_cs_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => down_cs_MC_Q
    );
  down_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_down_cs_MC_D_IN0,
      I1 => NlwBufferSignal_down_cs_MC_D_IN1,
      O => down_cs_MC_D_101
    );
  down_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_down_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_down_cs_MC_D1_IN1,
      O => down_cs_MC_D1_102
    );
  down_cs_MC_D2 : X_ZERO
    port map (
      O => down_cs_MC_D2_103
    );
  N_PZ_230 : X_BUF
    port map (
      I => N_PZ_230_MC_Q_104,
      O => N_PZ_230_76
    );
  N_PZ_230_MC_Q : X_BUF
    port map (
      I => N_PZ_230_MC_D_105,
      O => N_PZ_230_MC_Q_104
    );
  N_PZ_230_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_230_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_230_MC_D_IN1,
      O => N_PZ_230_MC_D_105
    );
  N_PZ_230_MC_D1 : X_ZERO
    port map (
      O => N_PZ_230_MC_D1_106
    );
  N_PZ_230_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_230_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_230_MC_D2_PT_0_IN1,
      O => N_PZ_230_MC_D2_PT_0_108
    );
  N_PZ_230_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_230_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_230_MC_D2_PT_1_IN1,
      O => N_PZ_230_MC_D2_PT_1_109
    );
  N_PZ_230_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_230_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_230_MC_D2_IN1,
      O => N_PZ_230_MC_D2_107
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q_110,
      O => cnt_10_MC_Q_37
    );
  cnt_10_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_10_MC_D_111,
      O => cnt_10_MC_Q_tsimrenamed_net_Q_110
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_111
    );
  cnt_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D1_IN1,
      O => cnt_10_MC_D1_112
    );
  cnt_10_MC_D2 : X_ZERO
    port map (
      O => cnt_10_MC_D2_113
    );
  cnt_10_BUFR : X_BUF
    port map (
      I => cnt_10_BUFR_MC_Q,
      O => cnt_10_BUFR_114
    );
  cnt_10_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_10_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_10_BUFR_MC_Q
    );
  cnt_10_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D_IN1,
      O => cnt_10_BUFR_MC_D_116
    );
  cnt_10_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_10_BUFR_MC_D1_117
    );
  cnt_10_BUFR_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN1,
      O => cnt_10_BUFR_MC_D2_PT_0_121
    );
  cnt_10_BUFR_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN3,
      O => cnt_10_BUFR_MC_D2_PT_1_122
    );
  cnt_10_BUFR_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN4,
      O => cnt_10_BUFR_MC_D2_PT_2_124
    );
  cnt_10_BUFR_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_3_IN4,
      O => cnt_10_BUFR_MC_D2_PT_3_126
    );
  cnt_10_BUFR_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN4,
      O => cnt_10_BUFR_MC_D2_PT_4_128
    );
  cnt_10_BUFR_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_5_IN4,
      O => cnt_10_BUFR_MC_D2_PT_5_130
    );
  cnt_10_BUFR_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN4,
      O => cnt_10_BUFR_MC_D2_PT_6_131
    );
  cnt_10_BUFR_MC_D2_PT_7 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN3,
      I4 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN5,
      I6 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN6,
      I7 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN7,
      O => cnt_10_BUFR_MC_D2_PT_7_132
    );
  cnt_10_BUFR_MC_D2_PT_8 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN1,
      I2 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_8_IN2,
      I3 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN4,
      I5 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN5,
      I6 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN6,
      I7 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN7,
      O => cnt_10_BUFR_MC_D2_PT_8_133
    );
  cnt_10_BUFR_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN15,
      O => cnt_10_BUFR_MC_D2_118
    );
  cnt_8_BUFR : X_BUF
    port map (
      I => cnt_8_BUFR_MC_Q,
      O => cnt_8_BUFR_127
    );
  cnt_8_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_8_BUFR_MC_Q
    );
  cnt_8_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D_IN1,
      O => cnt_8_BUFR_MC_D_135
    );
  cnt_8_BUFR_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN2,
      O => cnt_8_BUFR_MC_D1_136
    );
  cnt_8_BUFR_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN4,
      O => cnt_8_BUFR_MC_D2_PT_0_138
    );
  cnt_8_BUFR_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4,
      O => cnt_8_BUFR_MC_D2_PT_1_139
    );
  cnt_8_BUFR_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4,
      O => cnt_8_BUFR_MC_D2_PT_2_142
    );
  cnt_8_BUFR_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4,
      O => cnt_8_BUFR_MC_D2_PT_3_143
    );
  cnt_8_BUFR_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN4,
      O => cnt_8_BUFR_MC_D2_PT_4_144
    );
  cnt_8_BUFR_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN4,
      I5 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN5,
      O => cnt_8_BUFR_MC_D2_PT_5_145
    );
  cnt_8_BUFR_MC_D2_PT_6 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN5,
      O => cnt_8_BUFR_MC_D2_PT_6_146
    );
  cnt_8_BUFR_MC_D2 : X_OR7
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN6,
      O => cnt_8_BUFR_MC_D2_137
    );
  valLd_cs_7_Q : X_BUF
    port map (
      I => valLd_cs_7_MC_Q,
      O => valLd_cs(7)
    );
  valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_7_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_7_MC_Q
    );
  valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_7_MC_D_IN1,
      O => valLd_cs_7_MC_D_148
    );
  valLd_cs_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_7_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_7_MC_D1_IN1,
      O => valLd_cs_7_MC_D1_149
    );
  valLd_cs_7_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_7_MC_D2_150
    );
  valLd_cs_8_Q : X_BUF
    port map (
      I => valLd_cs_8_MC_Q,
      O => valLd_cs(8)
    );
  valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_8_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_8_MC_Q
    );
  valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_8_MC_D_IN1,
      O => valLd_cs_8_MC_D_152
    );
  valLd_cs_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_8_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_8_MC_D1_IN1,
      O => valLd_cs_8_MC_D1_153
    );
  valLd_cs_8_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_8_MC_D2_154
    );
  carry_s_7_Q : X_BUF
    port map (
      I => carry_s_7_MC_Q_155,
      O => carry_s(7)
    );
  carry_s_7_MC_Q : X_BUF
    port map (
      I => carry_s_7_MC_D_156,
      O => carry_s_7_MC_Q_155
    );
  carry_s_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_carry_s_7_MC_D_IN0,
      I1 => NlwBufferSignal_carry_s_7_MC_D_IN1,
      O => carry_s_7_MC_D_156
    );
  carry_s_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_carry_s_7_MC_D1_IN0,
      I1 => NlwBufferSignal_carry_s_7_MC_D1_IN1,
      O => carry_s_7_MC_D1_157
    );
  carry_s_7_MC_D2_PT_0 : X_AND7
    port map (
      I0 => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN6,
      O => carry_s_7_MC_D2_PT_0_164
    );
  carry_s_7_MC_D2_PT_1 : X_AND8
    port map (
      I0 => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_carry_s_7_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN7,
      O => carry_s_7_MC_D2_PT_1_167
    );
  carry_s_7_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN6,
      I7 => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN7,
      I8 => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN15,
      O => carry_s_7_MC_D2_PT_2_174
    );
  carry_s_7_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN15,
      O => carry_s_7_MC_D2_PT_3_175
    );
  carry_s_7_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_carry_s_7_MC_D2_IN0,
      I1 => NlwBufferSignal_carry_s_7_MC_D2_IN1,
      I2 => NlwBufferSignal_carry_s_7_MC_D2_IN2,
      I3 => NlwBufferSignal_carry_s_7_MC_D2_IN3,
      O => carry_s_7_MC_D2_158
    );
  valLd_cs_1_Q : X_BUF
    port map (
      I => valLd_cs_1_MC_Q,
      O => valLd_cs(1)
    );
  valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_1_MC_Q
    );
  valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_1_MC_D_IN1,
      O => valLd_cs_1_MC_D_177
    );
  valLd_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_1_MC_D1_IN1,
      O => valLd_cs_1_MC_D1_178
    );
  valLd_cs_1_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_1_MC_D2_179
    );
  valLd_cs_2_Q : X_BUF
    port map (
      I => valLd_cs_2_MC_Q,
      O => valLd_cs(2)
    );
  valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_2_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_2_MC_Q
    );
  valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_2_MC_D_IN1,
      O => valLd_cs_2_MC_D_181
    );
  valLd_cs_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_2_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_2_MC_D1_IN1,
      O => valLd_cs_2_MC_D1_182
    );
  valLd_cs_2_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_2_MC_D2_183
    );
  valLd_cs_3_Q : X_BUF
    port map (
      I => valLd_cs_3_MC_Q,
      O => valLd_cs(3)
    );
  valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_3_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_3_MC_Q
    );
  valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_3_MC_D_IN1,
      O => valLd_cs_3_MC_D_185
    );
  valLd_cs_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_3_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_3_MC_D1_IN1,
      O => valLd_cs_3_MC_D1_186
    );
  valLd_cs_3_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_3_MC_D2_187
    );
  valLd_cs_4_Q : X_BUF
    port map (
      I => valLd_cs_4_MC_Q,
      O => valLd_cs(4)
    );
  valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_4_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_4_MC_Q
    );
  valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_4_MC_D_IN1,
      O => valLd_cs_4_MC_D_189
    );
  valLd_cs_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_4_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_4_MC_D1_IN1,
      O => valLd_cs_4_MC_D1_190
    );
  valLd_cs_4_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_4_MC_D2_191
    );
  valLd_cs_5_Q : X_BUF
    port map (
      I => valLd_cs_5_MC_Q,
      O => valLd_cs(5)
    );
  valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_5_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_5_MC_Q
    );
  valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_5_MC_D_IN1,
      O => valLd_cs_5_MC_D_193
    );
  valLd_cs_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_5_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_5_MC_D1_IN1,
      O => valLd_cs_5_MC_D1_194
    );
  valLd_cs_5_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_5_MC_D2_195
    );
  valLd_cs_6_Q : X_BUF
    port map (
      I => valLd_cs_6_MC_Q,
      O => valLd_cs(6)
    );
  valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_6_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_6_MC_Q
    );
  valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_6_MC_D_IN1,
      O => valLd_cs_6_MC_D_197
    );
  valLd_cs_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_6_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_6_MC_D1_IN1,
      O => valLd_cs_6_MC_D1_198
    );
  valLd_cs_6_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_6_MC_D2_199
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_Q_47
    );
  cnt_4_MC_UIM : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_UIM_159
    );
  cnt_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_4_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_4_MC_Q_tsimrenamed_net_Q
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_201
    );
  cnt_4_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D1_IN2,
      O => cnt_4_MC_D1_202
    );
  cnt_4_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN4,
      O => cnt_4_MC_D2_PT_0_204
    );
  cnt_4_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN4,
      O => cnt_4_MC_D2_PT_1_205
    );
  cnt_4_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN4,
      O => cnt_4_MC_D2_PT_2_206
    );
  cnt_4_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN4,
      O => cnt_4_MC_D2_PT_3_207
    );
  cnt_4_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5,
      O => cnt_4_MC_D2_PT_4_208
    );
  cnt_4_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN5,
      O => cnt_4_MC_D2_PT_5_209
    );
  cnt_4_MC_D2_PT_6 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_6_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN5,
      I6 => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN6,
      I7 => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN7,
      O => cnt_4_MC_D2_PT_6_210
    );
  cnt_4_MC_D2_PT_7 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN5,
      I6 => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN6,
      I7 => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN7,
      O => cnt_4_MC_D2_PT_7_211
    );
  cnt_4_MC_D2_PT_8 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN4,
      I5 => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN5,
      I6 => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN6,
      I7 => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN7,
      I8 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN8,
      I9 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN9,
      I10 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN10,
      I11 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN11,
      I12 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN12,
      I13 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN13,
      I14 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN14,
      I15 => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN15,
      O => cnt_4_MC_D2_PT_8_212
    );
  cnt_4_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_4_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_4_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_4_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_4_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_4_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_4_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_4_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_4_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_4_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_4_MC_D2_IN15,
      O => cnt_4_MC_D2_203
    );
  cnt_3_BUFR : X_BUF
    port map (
      I => cnt_3_BUFR_MC_Q,
      O => cnt_3_BUFR_160
    );
  cnt_3_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_3_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_3_BUFR_MC_Q
    );
  cnt_3_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D_IN1,
      O => cnt_3_BUFR_MC_D_214
    );
  cnt_3_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_3_BUFR_MC_D1_215
    );
  cnt_3_BUFR_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN4,
      O => cnt_3_BUFR_MC_D2_PT_0_217
    );
  cnt_3_BUFR_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN4,
      O => cnt_3_BUFR_MC_D2_PT_1_218
    );
  cnt_3_BUFR_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN4,
      O => cnt_3_BUFR_MC_D2_PT_2_219
    );
  cnt_3_BUFR_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN4,
      O => cnt_3_BUFR_MC_D2_PT_3_220
    );
  cnt_3_BUFR_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN4,
      O => cnt_3_BUFR_MC_D2_PT_4_221
    );
  cnt_3_BUFR_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN4,
      O => cnt_3_BUFR_MC_D2_PT_5_222
    );
  cnt_3_BUFR_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN4,
      O => cnt_3_BUFR_MC_D2_PT_6_223
    );
  cnt_3_BUFR_MC_D2_PT_7 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN4,
      O => cnt_3_BUFR_MC_D2_PT_7_224
    );
  cnt_3_BUFR_MC_D2_PT_8 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_8_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN4,
      O => cnt_3_BUFR_MC_D2_PT_8_225
    );
  cnt_3_BUFR_MC_D2_PT_9 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN2,
      I3 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN4,
      O => cnt_3_BUFR_MC_D2_PT_9_226
    );
  cnt_3_BUFR_MC_D2_PT_10 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_10_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN2,
      I3 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_10_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN4,
      O => cnt_3_BUFR_MC_D2_PT_10_227
    );
  cnt_3_BUFR_MC_D2_PT_11 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_11_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_11_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN4,
      O => cnt_3_BUFR_MC_D2_PT_11_228
    );
  cnt_3_BUFR_MC_D2_PT_12 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN3,
      I4 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN4,
      I5 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN5,
      I6 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN6,
      O => cnt_3_BUFR_MC_D2_PT_12_229
    );
  cnt_3_BUFR_MC_D2_PT_13 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN3,
      I4 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN4,
      I5 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN5,
      I6 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN6,
      I7 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN7,
      O => cnt_3_BUFR_MC_D2_PT_13_230
    );
  cnt_3_BUFR_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN15,
      O => cnt_3_BUFR_MC_D2_216
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_43
    );
  cnt_2_MC_UIM : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_UIM_165
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_232
    );
  cnt_2_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D1_IN2,
      O => cnt_2_MC_D1_233
    );
  cnt_2_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3,
      O => cnt_2_MC_D2_PT_0_235
    );
  cnt_2_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3,
      O => cnt_2_MC_D2_PT_1_236
    );
  cnt_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3,
      O => cnt_2_MC_D2_PT_2_238
    );
  cnt_2_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4,
      O => cnt_2_MC_D2_PT_3_239
    );
  cnt_2_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4,
      O => cnt_2_MC_D2_PT_4_240
    );
  cnt_2_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN5,
      O => cnt_2_MC_D2_PT_5_241
    );
  cnt_2_MC_D2_PT_6 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN5,
      O => cnt_2_MC_D2_PT_6_242
    );
  cnt_2_MC_D2_PT_7 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN5,
      I6 => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN6,
      O => cnt_2_MC_D2_PT_7_243
    );
  cnt_2_MC_D2 : X_OR8
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_2_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_2_MC_D2_IN7,
      O => cnt_2_MC_D2_234
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_41
    );
  cnt_1_MC_UIM : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_UIM_237
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_245
    );
  cnt_1_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D1_IN2,
      O => cnt_1_MC_D1_246
    );
  cnt_1_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3,
      O => cnt_1_MC_D2_PT_0_248
    );
  cnt_1_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN4,
      O => cnt_1_MC_D2_PT_1_249
    );
  cnt_1_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4,
      O => cnt_1_MC_D2_PT_2_250
    );
  cnt_1_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN4,
      O => cnt_1_MC_D2_PT_3_251
    );
  cnt_1_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4,
      O => cnt_1_MC_D2_PT_4_252
    );
  cnt_1_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN4,
      I5 => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN5,
      O => cnt_1_MC_D2_PT_5_253
    );
  cnt_1_MC_D2_PT_6 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN4,
      I5 => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN5,
      O => cnt_1_MC_D2_PT_6_254
    );
  cnt_1_MC_D2 : X_OR7
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_1_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_1_MC_D2_IN6,
      O => cnt_1_MC_D2_247
    );
  N_PZ_211 : X_BUF
    port map (
      I => N_PZ_211_MC_Q_255,
      O => N_PZ_211_161
    );
  N_PZ_211_MC_Q : X_BUF
    port map (
      I => N_PZ_211_MC_D_256,
      O => N_PZ_211_MC_Q_255
    );
  N_PZ_211_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_211_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_211_MC_D_IN1,
      O => N_PZ_211_MC_D_256
    );
  N_PZ_211_MC_D1 : X_ZERO
    port map (
      O => N_PZ_211_MC_D1_257
    );
  N_PZ_211_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN2,
      O => N_PZ_211_MC_D2_PT_0_259
    );
  N_PZ_211_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN2,
      O => N_PZ_211_MC_D2_PT_1_260
    );
  N_PZ_211_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_211_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_211_MC_D2_IN1,
      O => N_PZ_211_MC_D2_258
    );
  N_PZ_214 : X_BUF
    port map (
      I => N_PZ_214_MC_Q_261,
      O => N_PZ_214_166
    );
  N_PZ_214_MC_Q : X_BUF
    port map (
      I => N_PZ_214_MC_D_262,
      O => N_PZ_214_MC_Q_261
    );
  N_PZ_214_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_214_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_214_MC_D_IN1,
      O => N_PZ_214_MC_D_262
    );
  N_PZ_214_MC_D1 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_214_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_214_MC_D1_IN1,
      I2 => NlwBufferSignal_N_PZ_214_MC_D1_IN2,
      O => N_PZ_214_MC_D1_263
    );
  N_PZ_214_MC_D2 : X_ZERO
    port map (
      O => N_PZ_214_MC_D2_264
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_Q_49
    );
  cnt_5_MC_UIM : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_UIM_162
    );
  cnt_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_5_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_5_MC_Q_tsimrenamed_net_Q
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_266
    );
  cnt_5_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D1_IN2,
      O => cnt_5_MC_D1_267
    );
  cnt_5_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN4,
      O => cnt_5_MC_D2_PT_0_269
    );
  cnt_5_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN4,
      O => cnt_5_MC_D2_PT_1_270
    );
  cnt_5_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4,
      O => cnt_5_MC_D2_PT_2_271
    );
  cnt_5_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4,
      O => cnt_5_MC_D2_PT_3_272
    );
  cnt_5_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4,
      O => cnt_5_MC_D2_PT_4_273
    );
  cnt_5_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN4,
      O => cnt_5_MC_D2_PT_5_274
    );
  cnt_5_MC_D2_PT_6 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN5,
      I6 => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN6,
      O => cnt_5_MC_D2_PT_6_275
    );
  cnt_5_MC_D2_PT_7 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_7_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN6,
      O => cnt_5_MC_D2_PT_7_276
    );
  cnt_5_MC_D2_PT_8 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_8_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_8_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN7,
      I8 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN15,
      O => cnt_5_MC_D2_PT_8_277
    );
  cnt_5_MC_D2_PT_9 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN5,
      I6 => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN6,
      I7 => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN7,
      I8 => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN15,
      O => cnt_5_MC_D2_PT_9_278
    );
  cnt_5_MC_D2_PT_10 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN5,
      I6 => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN6,
      I7 => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN7,
      I8 => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN15,
      O => cnt_5_MC_D2_PT_10_279
    );
  cnt_5_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_5_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_IN15,
      O => cnt_5_MC_D2_268
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_Q_51
    );
  cnt_6_MC_UIM : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_UIM_163
    );
  cnt_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_6_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_6_MC_Q_tsimrenamed_net_Q
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_281
    );
  cnt_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D1_IN1,
      O => cnt_6_MC_D1_282
    );
  cnt_6_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4,
      O => cnt_6_MC_D2_PT_0_284
    );
  cnt_6_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4,
      O => cnt_6_MC_D2_PT_1_285
    );
  cnt_6_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN4,
      O => cnt_6_MC_D2_PT_2_286
    );
  cnt_6_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4,
      O => cnt_6_MC_D2_PT_3_287
    );
  cnt_6_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7,
      O => cnt_6_MC_D2_PT_4_288
    );
  cnt_6_MC_D2_PT_5 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN7,
      I8 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN8,
      I9 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN15,
      O => cnt_6_MC_D2_PT_5_289
    );
  cnt_6_MC_D2_PT_6 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN7,
      I8 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN8,
      I9 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN15,
      O => cnt_6_MC_D2_PT_6_290
    );
  cnt_6_MC_D2_PT_7 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN7,
      I8 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN8,
      I9 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN15,
      O => cnt_6_MC_D2_PT_7_291
    );
  cnt_6_MC_D2 : X_OR8
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_IN7,
      O => cnt_6_MC_D2_283
    );
  cnt_7_BUFR : X_BUF
    port map (
      I => cnt_7_BUFR_MC_Q,
      O => cnt_7_BUFR_129
    );
  cnt_7_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_7_BUFR_MC_Q
    );
  cnt_7_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D_IN1,
      O => cnt_7_BUFR_MC_D_293
    );
  cnt_7_BUFR_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1,
      O => cnt_7_BUFR_MC_D1_294
    );
  cnt_7_BUFR_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_0_IN3,
      O => cnt_7_BUFR_MC_D2_PT_0_296
    );
  cnt_7_BUFR_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN3,
      O => cnt_7_BUFR_MC_D2_PT_1_297
    );
  cnt_7_BUFR_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN3,
      O => cnt_7_BUFR_MC_D2_PT_2_298
    );
  cnt_7_BUFR_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN3,
      O => cnt_7_BUFR_MC_D2_PT_3_299
    );
  cnt_7_BUFR_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN3,
      O => cnt_7_BUFR_MC_D2_295
    );
  cnt_9_BUFR : X_BUF
    port map (
      I => cnt_9_BUFR_MC_Q,
      O => cnt_9_BUFR_123
    );
  cnt_9_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_9_BUFR_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_9_BUFR_MC_Q
    );
  cnt_9_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_cnt_9_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D_IN1,
      O => cnt_9_BUFR_MC_D_301
    );
  cnt_9_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_9_BUFR_MC_D1_302
    );
  cnt_9_BUFR_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_0_IN1,
      O => cnt_9_BUFR_MC_D2_PT_0_304
    );
  cnt_9_BUFR_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN2,
      O => cnt_9_BUFR_MC_D2_PT_1_306
    );
  cnt_9_BUFR_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_2_IN2,
      O => cnt_9_BUFR_MC_D2_PT_2_307
    );
  cnt_9_BUFR_MC_D2_PT_3 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN2,
      O => cnt_9_BUFR_MC_D2_PT_3_308
    );
  cnt_9_BUFR_MC_D2_PT_4 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN2,
      O => cnt_9_BUFR_MC_D2_PT_4_309
    );
  cnt_9_BUFR_MC_D2_PT_5 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN3,
      O => cnt_9_BUFR_MC_D2_PT_5_310
    );
  cnt_9_BUFR_MC_D2_PT_6 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN3,
      O => cnt_9_BUFR_MC_D2_PT_6_311
    );
  cnt_9_BUFR_MC_D2_PT_7 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN3,
      O => cnt_9_BUFR_MC_D2_PT_7_312
    );
  cnt_9_BUFR_MC_D2_PT_8 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN1,
      I2 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN2,
      I3 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN3,
      O => cnt_9_BUFR_MC_D2_PT_8_313
    );
  cnt_9_BUFR_MC_D2_PT_9 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_9_IN1,
      I2 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN2,
      I3 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_9_IN3,
      O => cnt_9_BUFR_MC_D2_PT_9_314
    );
  cnt_9_BUFR_MC_D2_PT_10 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN0,
      I1 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN1,
      I2 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN2,
      I3 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN3,
      I4 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN4,
      O => cnt_9_BUFR_MC_D2_PT_10_315
    );
  cnt_9_BUFR_MC_D2_PT_11 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN1,
      I2 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN2,
      I3 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN3,
      I4 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN4,
      I5 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN5,
      O => cnt_9_BUFR_MC_D2_PT_11_316
    );
  cnt_9_BUFR_MC_D2_PT_12 : X_AND6
    port map (
      I0 => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_12_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN1,
      I2 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN2,
      I3 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN3,
      I4 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN4,
      I5 => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN5,
      O => cnt_9_BUFR_MC_D2_PT_12_317
    );
  cnt_9_BUFR_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN15,
      O => cnt_9_BUFR_MC_D2_303
    );
  valLd_cs_9_Q : X_BUF
    port map (
      I => valLd_cs_9_MC_Q,
      O => valLd_cs(9)
    );
  valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_9_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_9_MC_Q
    );
  valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_9_MC_D_IN1,
      O => valLd_cs_9_MC_D_319
    );
  valLd_cs_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_9_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_9_MC_D1_IN1,
      O => valLd_cs_9_MC_D1_320
    );
  valLd_cs_9_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_9_MC_D2_321
    );
  N_PZ_167 : X_BUF
    port map (
      I => N_PZ_167_MC_Q_322,
      O => N_PZ_167_119
    );
  N_PZ_167_MC_Q : X_BUF
    port map (
      I => N_PZ_167_MC_D_323,
      O => N_PZ_167_MC_Q_322
    );
  N_PZ_167_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_167_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_167_MC_D_IN1,
      O => N_PZ_167_MC_D_323
    );
  N_PZ_167_MC_D1 : X_ZERO
    port map (
      O => N_PZ_167_MC_D1_324
    );
  N_PZ_167_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_167_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_167_MC_D2_PT_0_IN1,
      O => N_PZ_167_MC_D2_PT_0_326
    );
  N_PZ_167_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_167_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_167_MC_D2_PT_1_IN1,
      O => N_PZ_167_MC_D2_PT_1_327
    );
  N_PZ_167_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN4,
      O => N_PZ_167_MC_D2_PT_2_328
    );
  N_PZ_167_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwInverterSignal_N_PZ_167_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN4,
      O => N_PZ_167_MC_D2_PT_3_329
    );
  N_PZ_167_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_N_PZ_167_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_167_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_167_MC_D2_IN2,
      I3 => NlwBufferSignal_N_PZ_167_MC_D2_IN3,
      O => N_PZ_167_MC_D2_325
    );
  valLd_cs_10_Q : X_BUF
    port map (
      I => valLd_cs_10_MC_Q,
      O => valLd_cs(10)
    );
  valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_10_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_10_MC_Q
    );
  valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_10_MC_D_IN1,
      O => valLd_cs_10_MC_D_331
    );
  valLd_cs_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_10_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_10_MC_D1_IN1,
      O => valLd_cs_10_MC_D1_332
    );
  valLd_cs_10_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_10_MC_D2_333
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_Q_39
    );
  cnt_11_MC_UIM : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_UIM_335
    );
  cnt_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_cnt_11_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => cnt_11_MC_Q_tsimrenamed_net_Q
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_336
    );
  cnt_11_MC_D1 : X_ZERO
    port map (
      O => cnt_11_MC_D1_337
    );
  cnt_11_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1,
      O => cnt_11_MC_D2_PT_0_340
    );
  cnt_11_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN4,
      O => cnt_11_MC_D2_PT_1_341
    );
  cnt_11_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4,
      O => cnt_11_MC_D2_PT_2_342
    );
  cnt_11_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4,
      O => cnt_11_MC_D2_PT_3_343
    );
  cnt_11_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4,
      O => cnt_11_MC_D2_PT_4_344
    );
  cnt_11_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4,
      O => cnt_11_MC_D2_PT_5_345
    );
  cnt_11_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN4,
      O => cnt_11_MC_D2_PT_6_346
    );
  cnt_11_MC_D2_PT_7 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN4,
      O => cnt_11_MC_D2_PT_7_347
    );
  cnt_11_MC_D2_PT_8 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_8_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN4,
      O => cnt_11_MC_D2_PT_8_348
    );
  cnt_11_MC_D2_PT_9 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN4,
      O => cnt_11_MC_D2_PT_9_349
    );
  cnt_11_MC_D2_PT_10 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_10_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_10_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN4,
      O => cnt_11_MC_D2_PT_10_350
    );
  cnt_11_MC_D2_PT_11 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_11_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_11_IN5,
      O => cnt_11_MC_D2_PT_11_351
    );
  cnt_11_MC_D2_PT_12 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_12_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_12_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_12_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_12_IN5,
      O => cnt_11_MC_D2_PT_12_352
    );
  cnt_11_MC_D2_PT_13 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN5,
      I6 => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN6,
      I7 => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN7,
      I8 => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN8,
      I9 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN9,
      I10 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN10,
      I11 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN11,
      I12 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN12,
      I13 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN13,
      I14 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN15,
      O => cnt_11_MC_D2_PT_13_353
    );
  cnt_11_MC_D2_PT_14 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_14_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN6,
      I7 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN7,
      I8 => NlwInverterSignal_cnt_11_MC_D2_PT_14_IN8,
      I9 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN9,
      I10 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN10,
      I11 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN11,
      I12 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN12,
      I13 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN13,
      I14 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN15,
      O => cnt_11_MC_D2_PT_14_354
    );
  cnt_11_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_11_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_11_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_11_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_11_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_11_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_11_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_11_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_11_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_IN15,
      O => cnt_11_MC_D2_338
    );
  valLd_cs_11_Q : X_BUF
    port map (
      I => valLd_cs_11_MC_Q,
      O => valLd_cs(11)
    );
  valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_11_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => valLd_cs_11_MC_Q
    );
  valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_11_MC_D_IN1,
      O => valLd_cs_11_MC_D_356
    );
  valLd_cs_11_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_11_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_11_MC_D1_IN1,
      O => valLd_cs_11_MC_D1_357
    );
  valLd_cs_11_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_11_MC_D2_358
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q_359,
      O => cnt_3_MC_Q_45
    );
  cnt_3_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_3_MC_D_360,
      O => cnt_3_MC_Q_tsimrenamed_net_Q_359
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_360
    );
  cnt_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D1_IN1,
      O => cnt_3_MC_D1_361
    );
  cnt_3_MC_D2 : X_ZERO
    port map (
      O => cnt_3_MC_D2_362
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q_363,
      O => cnt_7_MC_Q_53
    );
  cnt_7_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_7_MC_D_364,
      O => cnt_7_MC_Q_tsimrenamed_net_Q_363
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_364
    );
  cnt_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D1_IN1,
      O => cnt_7_MC_D1_365
    );
  cnt_7_MC_D2 : X_ZERO
    port map (
      O => cnt_7_MC_D2_366
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q_367,
      O => cnt_8_MC_Q_55
    );
  cnt_8_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_8_MC_D_368,
      O => cnt_8_MC_Q_tsimrenamed_net_Q_367
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_368
    );
  cnt_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D1_IN1,
      O => cnt_8_MC_D1_369
    );
  cnt_8_MC_D2 : X_ZERO
    port map (
      O => cnt_8_MC_D2_370
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q_371,
      O => cnt_9_MC_Q_57
    );
  cnt_9_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_9_MC_D_372,
      O => cnt_9_MC_Q_tsimrenamed_net_Q_371
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_372
    );
  cnt_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D1_IN1,
      O => cnt_9_MC_D1_373
    );
  cnt_9_MC_D2 : X_ZERO
    port map (
      O => cnt_9_MC_D2_374
    );
  did_0_MC_Q : X_BUF
    port map (
      I => did_0_MC_Q_tsimrenamed_net_Q_375,
      O => did_0_MC_Q_59
    );
  did_0_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_0_MC_D_376,
      O => did_0_MC_Q_tsimrenamed_net_Q_375
    );
  did_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_0_MC_D_IN0,
      I1 => NlwBufferSignal_did_0_MC_D_IN1,
      O => did_0_MC_D_376
    );
  did_0_MC_D1 : X_ZERO
    port map (
      O => did_0_MC_D1_377
    );
  did_0_MC_D2 : X_ZERO
    port map (
      O => did_0_MC_D2_378
    );
  did_1_MC_Q : X_BUF
    port map (
      I => did_1_MC_Q_tsimrenamed_net_Q,
      O => did_1_MC_Q_61
    );
  did_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_did_1_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_did_1_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => did_1_MC_Q_tsimrenamed_net_Q
    );
  did_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_1_MC_D_IN0,
      I1 => NlwBufferSignal_did_1_MC_D_IN1,
      O => did_1_MC_D_380
    );
  did_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_did_1_MC_D1_IN0,
      I1 => NlwBufferSignal_did_1_MC_D1_IN1,
      O => did_1_MC_D1_381
    );
  did_1_MC_D2 : X_ZERO
    port map (
      O => did_1_MC_D2_382
    );
  did_2_MC_Q : X_BUF
    port map (
      I => did_2_MC_Q_tsimrenamed_net_Q_383,
      O => did_2_MC_Q_63
    );
  did_2_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_2_MC_D_384,
      O => did_2_MC_Q_tsimrenamed_net_Q_383
    );
  did_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_2_MC_D_IN0,
      I1 => NlwBufferSignal_did_2_MC_D_IN1,
      O => did_2_MC_D_384
    );
  did_2_MC_D1 : X_ZERO
    port map (
      O => did_2_MC_D1_385
    );
  did_2_MC_D2 : X_ZERO
    port map (
      O => did_2_MC_D2_386
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_65
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => Vcc_70,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_69,
      RST => Gnd_69,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_388
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_389
    );
  err_MC_D2_PT_0 : X_AND6
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_0_IN5,
      O => err_MC_D2_PT_0_391
    );
  err_MC_D2_PT_1 : X_AND6
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_1_IN5,
      O => err_MC_D2_PT_1_392
    );
  err_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_err_MC_D2_PT_2_IN6,
      I7 => NlwInverterSignal_err_MC_D2_PT_2_IN7,
      I8 => NlwBufferSignal_err_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_2_IN15,
      O => err_MC_D2_PT_2_393
    );
  err_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_err_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_err_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_3_IN15,
      O => err_MC_D2_PT_3_394
    );
  err_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_IN3,
      O => err_MC_D2_390
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_D_68,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_71,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_72,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_0_77,
      O => NlwBufferSignal_cnt_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_1_78,
      O => NlwBufferSignal_cnt_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_2_80,
      O => NlwBufferSignal_cnt_0_MC_D2_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_3_81,
      O => NlwBufferSignal_cnt_0_MC_D2_IN3
    );
  NlwBufferBlock_cnt_0_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_4_82,
      O => NlwBufferSignal_cnt_0_MC_D2_IN4
    );
  NlwBufferBlock_nLd_cs_MC_REG_IN : X_BUF
    port map (
      I => nLd_cs_MC_D_84,
      O => NlwBufferSignal_nLd_cs_MC_REG_IN
    );
  NlwBufferBlock_nLd_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_nLd_cs_MC_REG_CLK
    );
  NlwBufferBlock_nLd_cs_MC_D_IN0 : X_BUF
    port map (
      I => nLd_cs_MC_D1_85,
      O => NlwBufferSignal_nLd_cs_MC_D_IN0
    );
  NlwBufferBlock_nLd_cs_MC_D_IN1 : X_BUF
    port map (
      I => nLd_cs_MC_D2_86,
      O => NlwBufferSignal_nLd_cs_MC_D_IN1
    );
  NlwBufferBlock_nLd_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_nLd_cs_MC_D1_IN0
    );
  NlwBufferBlock_nLd_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_5,
      O => NlwBufferSignal_nLd_cs_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_0_MC_D_88,
      O => NlwBufferSignal_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_0_MC_D1_89,
      O => NlwBufferSignal_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_0_MC_D2_90,
      O => NlwBufferSignal_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_0_II_UIM_7,
      O => NlwBufferSignal_valLd_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_up_cs_MC_REG_IN : X_BUF
    port map (
      I => up_cs_MC_D_92,
      O => NlwBufferSignal_up_cs_MC_REG_IN
    );
  NlwBufferBlock_up_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_up_cs_MC_REG_CLK
    );
  NlwBufferBlock_up_cs_MC_D_IN0 : X_BUF
    port map (
      I => up_cs_MC_D1_93,
      O => NlwBufferSignal_up_cs_MC_D_IN0
    );
  NlwBufferBlock_up_cs_MC_D_IN1 : X_BUF
    port map (
      I => up_cs_MC_D2_94,
      O => NlwBufferSignal_up_cs_MC_D_IN1
    );
  NlwBufferBlock_up_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_up_cs_MC_D1_IN0
    );
  NlwBufferBlock_up_cs_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_9,
      O => NlwBufferSignal_up_cs_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_199_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_199_MC_D1_97,
      O => NlwBufferSignal_N_PZ_199_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_199_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_199_MC_D2_98,
      O => NlwBufferSignal_N_PZ_199_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_199_MC_D1_IN0 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_N_PZ_199_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_199_MC_D1_IN1 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_N_PZ_199_MC_D1_IN1
    );
  NlwBufferBlock_down_cs_MC_REG_IN : X_BUF
    port map (
      I => down_cs_MC_D_101,
      O => NlwBufferSignal_down_cs_MC_REG_IN
    );
  NlwBufferBlock_down_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_down_cs_MC_REG_CLK
    );
  NlwBufferBlock_down_cs_MC_D_IN0 : X_BUF
    port map (
      I => down_cs_MC_D1_102,
      O => NlwBufferSignal_down_cs_MC_D_IN0
    );
  NlwBufferBlock_down_cs_MC_D_IN1 : X_BUF
    port map (
      I => down_cs_MC_D2_103,
      O => NlwBufferSignal_down_cs_MC_D_IN1
    );
  NlwBufferBlock_down_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_down_cs_MC_D1_IN0
    );
  NlwBufferBlock_down_cs_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_11,
      O => NlwBufferSignal_down_cs_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_230_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_230_MC_D1_106,
      O => NlwBufferSignal_N_PZ_230_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_230_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_230_MC_D2_107,
      O => NlwBufferSignal_N_PZ_230_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_230_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_N_PZ_230_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_230_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_N_PZ_230_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_230_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_N_PZ_230_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_230_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_N_PZ_230_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_230_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_230_MC_D2_PT_0_108,
      O => NlwBufferSignal_N_PZ_230_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_230_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_230_MC_D2_PT_1_109,
      O => NlwBufferSignal_N_PZ_230_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_112,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_113,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_MC_D1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D_116,
      O => NlwBufferSignal_cnt_10_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D1_117,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_118,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_7_IN7 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN7
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_8_IN5 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN5
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_8_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN6
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_8_IN7 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN7
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_0_121,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_1_122,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_2_124,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_3_126,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_4_128,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN4
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_5_130,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN5
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_6_131,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN6
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_7_132,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN7
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_8_133,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN8
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN9 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN9
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN10
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN11
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN12
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN13
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN14
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN15
    );
  NlwBufferBlock_cnt_8_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D_135,
      O => NlwBufferSignal_cnt_8_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D1_136,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_137,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_0_138,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_1_139,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_2_142,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_3_143,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_4_144,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_5_145,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN5
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_6_146,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN6
    );
  NlwBufferBlock_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_7_MC_D_148,
      O => NlwBufferSignal_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_7_MC_D1_149,
      O => NlwBufferSignal_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_7_MC_D2_150,
      O => NlwBufferSignal_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_7_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_7_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_7_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_7_II_UIM_13,
      O => NlwBufferSignal_valLd_cs_7_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_8_MC_D_152,
      O => NlwBufferSignal_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_8_MC_D1_153,
      O => NlwBufferSignal_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_8_MC_D2_154,
      O => NlwBufferSignal_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_8_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_8_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_8_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_8_II_UIM_15,
      O => NlwBufferSignal_valLd_cs_8_MC_D1_IN1
    );
  NlwBufferBlock_carry_s_7_MC_D_IN0 : X_BUF
    port map (
      I => carry_s_7_MC_D1_157,
      O => NlwBufferSignal_carry_s_7_MC_D_IN0
    );
  NlwBufferBlock_carry_s_7_MC_D_IN1 : X_BUF
    port map (
      I => carry_s_7_MC_D2_158,
      O => NlwBufferSignal_carry_s_7_MC_D_IN1
    );
  NlwBufferBlock_carry_s_7_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_carry_s_7_MC_D1_IN0
    );
  NlwBufferBlock_carry_s_7_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_carry_s_7_MC_D1_IN1
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_6_MC_UIM_163,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_6_MC_UIM_163,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => N_PZ_214_166,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_carry_s_7_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_carry_s_7_MC_D2_IN0 : X_BUF
    port map (
      I => carry_s_7_MC_D2_PT_0_164,
      O => NlwBufferSignal_carry_s_7_MC_D2_IN0
    );
  NlwBufferBlock_carry_s_7_MC_D2_IN1 : X_BUF
    port map (
      I => carry_s_7_MC_D2_PT_1_167,
      O => NlwBufferSignal_carry_s_7_MC_D2_IN1
    );
  NlwBufferBlock_carry_s_7_MC_D2_IN2 : X_BUF
    port map (
      I => carry_s_7_MC_D2_PT_2_174,
      O => NlwBufferSignal_carry_s_7_MC_D2_IN2
    );
  NlwBufferBlock_carry_s_7_MC_D2_IN3 : X_BUF
    port map (
      I => carry_s_7_MC_D2_PT_3_175,
      O => NlwBufferSignal_carry_s_7_MC_D2_IN3
    );
  NlwBufferBlock_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_1_MC_D_177,
      O => NlwBufferSignal_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_1_MC_D1_178,
      O => NlwBufferSignal_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_1_MC_D2_179,
      O => NlwBufferSignal_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_1_II_UIM_17,
      O => NlwBufferSignal_valLd_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_2_MC_D_181,
      O => NlwBufferSignal_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_2_MC_D1_182,
      O => NlwBufferSignal_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_2_MC_D2_183,
      O => NlwBufferSignal_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_2_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_2_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_2_II_UIM_19,
      O => NlwBufferSignal_valLd_cs_2_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_3_MC_D_185,
      O => NlwBufferSignal_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_3_MC_D1_186,
      O => NlwBufferSignal_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_3_MC_D2_187,
      O => NlwBufferSignal_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_3_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_3_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_3_II_UIM_21,
      O => NlwBufferSignal_valLd_cs_3_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_4_MC_D_189,
      O => NlwBufferSignal_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_4_MC_D1_190,
      O => NlwBufferSignal_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_4_MC_D2_191,
      O => NlwBufferSignal_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_4_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_4_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_4_II_UIM_23,
      O => NlwBufferSignal_valLd_cs_4_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_5_MC_D_193,
      O => NlwBufferSignal_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_5_MC_D1_194,
      O => NlwBufferSignal_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_5_MC_D2_195,
      O => NlwBufferSignal_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_5_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_5_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_5_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_5_II_UIM_25,
      O => NlwBufferSignal_valLd_cs_5_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_6_MC_D_197,
      O => NlwBufferSignal_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_6_MC_D1_198,
      O => NlwBufferSignal_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_6_MC_D2_199,
      O => NlwBufferSignal_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_6_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_6_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_6_II_UIM_27,
      O => NlwBufferSignal_valLd_cs_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_MC_D_201,
      O => NlwBufferSignal_cnt_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_202,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_203,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D1_IN2 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D1_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_214_166,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => N_PZ_214_166,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_6_IN6 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_6_IN7 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_7_IN7 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN6 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN7 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN8 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN8
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN9
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN10
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN11
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN12
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN13
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN14
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_8_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN15
    );
  NlwBufferBlock_cnt_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_0_204,
      O => NlwBufferSignal_cnt_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_1_205,
      O => NlwBufferSignal_cnt_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_2_206,
      O => NlwBufferSignal_cnt_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_3_207,
      O => NlwBufferSignal_cnt_4_MC_D2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_4_208,
      O => NlwBufferSignal_cnt_4_MC_D2_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_5_209,
      O => NlwBufferSignal_cnt_4_MC_D2_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_6_210,
      O => NlwBufferSignal_cnt_4_MC_D2_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_7_211,
      O => NlwBufferSignal_cnt_4_MC_D2_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_8_212,
      O => NlwBufferSignal_cnt_4_MC_D2_IN8
    );
  NlwBufferBlock_cnt_4_MC_D2_IN9 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_4_MC_D2_IN9
    );
  NlwBufferBlock_cnt_4_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_4_MC_D2_IN10
    );
  NlwBufferBlock_cnt_4_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_4_MC_D2_IN11
    );
  NlwBufferBlock_cnt_4_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_4_MC_D2_IN12
    );
  NlwBufferBlock_cnt_4_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_4_MC_D2_IN13
    );
  NlwBufferBlock_cnt_4_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_4_MC_D2_IN14
    );
  NlwBufferBlock_cnt_4_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_4_MC_D2_IN15
    );
  NlwBufferBlock_cnt_3_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D_214,
      O => NlwBufferSignal_cnt_3_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D1_215,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_216,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_214_166,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_214_166,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_10_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_10_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_10_IN2 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_10_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_10_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_11_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_11_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_11_IN2 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_11_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_11_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_12_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_12_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_12_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_12_IN3 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_12_IN4 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_12_IN5 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN5
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_12_IN6 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN6
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_13_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_13_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_13_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_13_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_13_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_13_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN5
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_13_IN6 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN6
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_13_IN7 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN7
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_0_217,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_1_218,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_2_219,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_3_220,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_4_221,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_5_222,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN5
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_6_223,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN6
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_7_224,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN7
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_8_225,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN8
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN9 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_9_226,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN9
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN10 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_10_227,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN10
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN11 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_11_228,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN11
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN12 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_12_229,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN12
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN13 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_13_230,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN13
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN14
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN15
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_D_232,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_233,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_234,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D1_IN2 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_235,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_236,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_238,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_3_239,
      O => NlwBufferSignal_cnt_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_4_240,
      O => NlwBufferSignal_cnt_2_MC_D2_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_5_241,
      O => NlwBufferSignal_cnt_2_MC_D2_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_6_242,
      O => NlwBufferSignal_cnt_2_MC_D2_IN6
    );
  NlwBufferBlock_cnt_2_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_7_243,
      O => NlwBufferSignal_cnt_2_MC_D2_IN7
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_D_245,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_246,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_247,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D1_IN2 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_248,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_249,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_2_250,
      O => NlwBufferSignal_cnt_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_3_251,
      O => NlwBufferSignal_cnt_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_4_252,
      O => NlwBufferSignal_cnt_1_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_5_253,
      O => NlwBufferSignal_cnt_1_MC_D2_IN5
    );
  NlwBufferBlock_cnt_1_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_6_254,
      O => NlwBufferSignal_cnt_1_MC_D2_IN6
    );
  NlwBufferBlock_N_PZ_211_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_211_MC_D1_257,
      O => NlwBufferSignal_N_PZ_211_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_211_MC_D2_258,
      O => NlwBufferSignal_N_PZ_211_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_211_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_211_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_211_MC_D2_PT_0_259,
      O => NlwBufferSignal_N_PZ_211_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_211_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_211_MC_D2_PT_1_260,
      O => NlwBufferSignal_N_PZ_211_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_214_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_214_MC_D1_263,
      O => NlwBufferSignal_N_PZ_214_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_214_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_214_MC_D2_264,
      O => NlwBufferSignal_N_PZ_214_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_214_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_230_76,
      O => NlwBufferSignal_N_PZ_214_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_214_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_67,
      O => NlwBufferSignal_N_PZ_214_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_214_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_237,
      O => NlwBufferSignal_N_PZ_214_MC_D1_IN2
    );
  NlwBufferBlock_cnt_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_MC_D_266,
      O => NlwBufferSignal_cnt_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_267,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_268,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D1_IN2 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D1_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => N_PZ_214_166,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN6 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => N_PZ_214_166,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN6 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN7 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN8 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_8_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN5 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN6 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN7 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN8 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_9_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN3 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN4 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN6 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN7 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN8 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN9 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_10_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_0_269,
      O => NlwBufferSignal_cnt_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_1_270,
      O => NlwBufferSignal_cnt_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_2_271,
      O => NlwBufferSignal_cnt_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_3_272,
      O => NlwBufferSignal_cnt_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_4_273,
      O => NlwBufferSignal_cnt_5_MC_D2_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_5_274,
      O => NlwBufferSignal_cnt_5_MC_D2_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_6_275,
      O => NlwBufferSignal_cnt_5_MC_D2_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_7_276,
      O => NlwBufferSignal_cnt_5_MC_D2_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_8_277,
      O => NlwBufferSignal_cnt_5_MC_D2_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_IN9 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_9_278,
      O => NlwBufferSignal_cnt_5_MC_D2_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_IN10 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_10_279,
      O => NlwBufferSignal_cnt_5_MC_D2_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_MC_D2_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_MC_D2_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_MC_D2_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_MC_D2_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_5_MC_D2_IN15
    );
  NlwBufferBlock_cnt_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_MC_D_281,
      O => NlwBufferSignal_cnt_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_282,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_283,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_6_MC_UIM_163,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_6_MC_UIM_163,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => N_PZ_211_161,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_4_MC_UIM_159,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN6 : X_BUF
    port map (
      I => cnt_5_MC_UIM_162,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN7 : X_BUF
    port map (
      I => cnt_2_MC_UIM_165,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN8 : X_BUF
    port map (
      I => N_PZ_214_166,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN6 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN7 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN8 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN9 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => up_cs_79,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => down_cs_99,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN7 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN8 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN9 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN10 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN11 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_0_284,
      O => NlwBufferSignal_cnt_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_1_285,
      O => NlwBufferSignal_cnt_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_2_286,
      O => NlwBufferSignal_cnt_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_3_287,
      O => NlwBufferSignal_cnt_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_4_288,
      O => NlwBufferSignal_cnt_6_MC_D2_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_5_289,
      O => NlwBufferSignal_cnt_6_MC_D2_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_6_290,
      O => NlwBufferSignal_cnt_6_MC_D2_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_7_291,
      O => NlwBufferSignal_cnt_6_MC_D2_IN7
    );
  NlwBufferBlock_cnt_7_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D_293,
      O => NlwBufferSignal_cnt_7_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D1_294,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_295,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_0_296,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_1_297,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_2_298,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_PT_3_299,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D_301,
      O => NlwBufferSignal_cnt_9_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D1_302,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_303,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_10_IN0 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_10_IN1 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_10_IN2 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_10_IN3 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_10_IN4 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN4
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_11_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_11_IN1 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_11_IN2 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_11_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_11_IN4 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN4
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_11_IN5 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN5
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_12_IN0 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_12_IN1 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_12_IN2 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_12_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_12_IN4 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN4
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_PT_12_IN5 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN5
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_0_304,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_1_306,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_2_307,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_3_308,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_4_309,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN4
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_5_310,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN5
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_6_311,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN6
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_7_312,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN7
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_8_313,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN8
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN9 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_9_314,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN9
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN10 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_10_315,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN10
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN11 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_11_316,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN11
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN12 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_PT_12_317,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN12
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN13
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN14
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D2_IN15
    );
  NlwBufferBlock_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_9_MC_D_319,
      O => NlwBufferSignal_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_9_MC_D1_320,
      O => NlwBufferSignal_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_9_MC_D2_321,
      O => NlwBufferSignal_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_9_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_9_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_9_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_9_II_UIM_29,
      O => NlwBufferSignal_valLd_cs_9_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_167_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_167_MC_D1_324,
      O => NlwBufferSignal_N_PZ_167_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_167_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_167_MC_D2_325,
      O => NlwBufferSignal_N_PZ_167_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_N_PZ_167_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_N_PZ_167_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_167_MC_D2_PT_0_326,
      O => NlwBufferSignal_N_PZ_167_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_167_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_167_MC_D2_PT_1_327,
      O => NlwBufferSignal_N_PZ_167_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_167_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_167_MC_D2_PT_2_328,
      O => NlwBufferSignal_N_PZ_167_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_167_MC_D2_IN3 : X_BUF
    port map (
      I => N_PZ_167_MC_D2_PT_3_329,
      O => NlwBufferSignal_N_PZ_167_MC_D2_IN3
    );
  NlwBufferBlock_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_10_MC_D_331,
      O => NlwBufferSignal_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_10_MC_D1_332,
      O => NlwBufferSignal_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_10_MC_D2_333,
      O => NlwBufferSignal_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_10_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_10_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_10_II_UIM_31,
      O => NlwBufferSignal_valLd_cs_10_MC_D1_IN1
    );
  NlwBufferBlock_cnt_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_MC_D_336,
      O => NlwBufferSignal_cnt_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_337,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_338,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_10_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_10_IN1 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_10_IN2 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_10_IN3 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_10_IN4 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_11_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_11_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_11_IN2 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_11_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_11_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_11_IN5 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_12_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_12_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_12_IN2 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_12_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_12_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_12_IN5 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN4 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN5 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN7 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN8 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_13_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN15
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN4 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN5 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN7 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN8 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_14_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN15
    );
  NlwBufferBlock_cnt_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_0_340,
      O => NlwBufferSignal_cnt_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_1_341,
      O => NlwBufferSignal_cnt_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_2_342,
      O => NlwBufferSignal_cnt_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_3_343,
      O => NlwBufferSignal_cnt_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_4_344,
      O => NlwBufferSignal_cnt_11_MC_D2_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_5_345,
      O => NlwBufferSignal_cnt_11_MC_D2_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_6_346,
      O => NlwBufferSignal_cnt_11_MC_D2_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_7_347,
      O => NlwBufferSignal_cnt_11_MC_D2_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_8_348,
      O => NlwBufferSignal_cnt_11_MC_D2_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_IN9 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_9_349,
      O => NlwBufferSignal_cnt_11_MC_D2_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_IN10 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_10_350,
      O => NlwBufferSignal_cnt_11_MC_D2_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_IN11 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_11_351,
      O => NlwBufferSignal_cnt_11_MC_D2_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_IN12 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_12_352,
      O => NlwBufferSignal_cnt_11_MC_D2_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_IN13 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_13_353,
      O => NlwBufferSignal_cnt_11_MC_D2_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_IN14 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_14_354,
      O => NlwBufferSignal_cnt_11_MC_D2_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_69,
      O => NlwBufferSignal_cnt_11_MC_D2_IN15
    );
  NlwBufferBlock_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_11_MC_D_356,
      O => NlwBufferSignal_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_11_MC_D1_357,
      O => NlwBufferSignal_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_11_MC_D2_358,
      O => NlwBufferSignal_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_11_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_11_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_11_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_valLd_cs_11_MC_D1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_361,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_362,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_3_MC_D1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_3_BUFR_160,
      O => NlwBufferSignal_cnt_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_365,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_366,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_7_MC_D1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_cnt_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_369,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_370,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_8_MC_D1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_cnt_8_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_373,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_374,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_9_MC_D1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_cnt_9_MC_D1_IN1
    );
  NlwBufferBlock_did_0_MC_D_IN0 : X_BUF
    port map (
      I => did_0_MC_D1_377,
      O => NlwBufferSignal_did_0_MC_D_IN0
    );
  NlwBufferBlock_did_0_MC_D_IN1 : X_BUF
    port map (
      I => did_0_MC_D2_378,
      O => NlwBufferSignal_did_0_MC_D_IN1
    );
  NlwBufferBlock_did_1_MC_REG_IN : X_BUF
    port map (
      I => did_1_MC_D_380,
      O => NlwBufferSignal_did_1_MC_REG_IN
    );
  NlwBufferBlock_did_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_did_1_MC_REG_CLK
    );
  NlwBufferBlock_did_1_MC_D_IN0 : X_BUF
    port map (
      I => did_1_MC_D1_381,
      O => NlwBufferSignal_did_1_MC_D_IN0
    );
  NlwBufferBlock_did_1_MC_D_IN1 : X_BUF
    port map (
      I => did_1_MC_D2_382,
      O => NlwBufferSignal_did_1_MC_D_IN1
    );
  NlwBufferBlock_did_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_did_1_MC_D1_IN0
    );
  NlwBufferBlock_did_1_MC_D1_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_did_1_MC_D1_IN1
    );
  NlwBufferBlock_did_2_MC_D_IN0 : X_BUF
    port map (
      I => did_2_MC_D1_385,
      O => NlwBufferSignal_did_2_MC_D_IN0
    );
  NlwBufferBlock_did_2_MC_D_IN1 : X_BUF
    port map (
      I => did_2_MC_D2_386,
      O => NlwBufferSignal_did_2_MC_D_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_388,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_389,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_390,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_err_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_err_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_err_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_167_119,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_err_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_err_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_err_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_199_75,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_10_BUFR_114,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_9_BUFR_123,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => carry_s(7),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_8_BUFR_127,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_7_BUFR_129,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_11_MC_UIM_335,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_70,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => err_MC_D2_PT_0_391,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => err_MC_D2_PT_1_392,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_D2_IN2 : X_BUF
    port map (
      I => err_MC_D2_PT_2_393,
      O => NlwBufferSignal_err_MC_D2_IN2
    );
  NlwBufferBlock_err_MC_D2_IN3 : X_BUF
    port map (
      I => err_MC_D2_PT_3_394,
      O => NlwBufferSignal_err_MC_D2_IN3
    );
  NlwInverterBlock_cnt_0_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D1_IN1,
      O => NlwInverterSignal_cnt_0_MC_D1_IN1
    );
  NlwInverterBlock_cnt_0_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D1_IN2,
      O => NlwInverterSignal_cnt_0_MC_D1_IN2
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_N_PZ_199_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_199_MC_D1_IN0,
      O => NlwInverterSignal_N_PZ_199_MC_D1_IN0
    );
  NlwInverterBlock_N_PZ_230_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_230_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_230_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_7_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN4,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN4
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_7_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN6,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN6
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_7_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_7_IN7,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_7_IN7
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_8_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN2,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_8_IN2
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_8_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_8_IN3,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_8_IN3
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_5_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_5_IN5,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_5_IN5
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_2_IN7 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN7,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN7
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_2_IN8 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_2_IN8,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_2_IN8
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_carry_s_7_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_carry_s_7_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_carry_s_7_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_4_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D1_IN1,
      O => NlwInverterSignal_cnt_4_MC_D1_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_7_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_7_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN6,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN6
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_7_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_7_IN7,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_7_IN7
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_8_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_8_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_8_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN5,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN5
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_8_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN6,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN6
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_8_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_8_IN7,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_8_IN7
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_8_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_8_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_8_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_9_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_9_IN3,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_9_IN3
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_10_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_10_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_10_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_10_IN3,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_10_IN3
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_11_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_11_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_11_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_11_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_11_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_12_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_12_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_12_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN4,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN4
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_12_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN5,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN5
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_12_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_12_IN6,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_12_IN6
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_13_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_13_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN4,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN4
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_13_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_13_IN7,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_13_IN7
    );
  NlwInverterBlock_cnt_2_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D1_IN1,
      O => NlwInverterSignal_cnt_2_MC_D1_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_cnt_1_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D1_IN1,
      O => NlwInverterSignal_cnt_1_MC_D1_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_5_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN5,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN5
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_N_PZ_211_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_N_PZ_211_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_211_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_N_PZ_211_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_N_PZ_211_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_N_PZ_211_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_211_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_211_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_211_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_214_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_214_MC_D1_IN0,
      O => NlwInverterSignal_N_PZ_214_MC_D1_IN0
    );
  NlwInverterBlock_cnt_5_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D1_IN1,
      O => NlwInverterSignal_cnt_5_MC_D1_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_6_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN6,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN6
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_7_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_7_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_8_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_8_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_8_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_9_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_9_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN5
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_9_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN6,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN6
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_9_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN7,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN7
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_9_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_9_IN8,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_9_IN8
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_10_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_10_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_10_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_10_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN5
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_10_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN6,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN6
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_10_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN7,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN7
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_10_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_10_IN8,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_10_IN8
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN7
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN8,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN8
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN9,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN9
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN6
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_7_BUFR_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_7_BUFR_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D_IN0,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D_IN0
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_7_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN0,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN0
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_8_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN0,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN0
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_8_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN2,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_8_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_8_IN3,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_8_IN3
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_9_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_9_IN3,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_9_IN3
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_10_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN1,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN1
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_10_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN2,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN2
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_10_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN3,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN3
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_10_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_10_IN4,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_10_IN4
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_11_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN3,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN3
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_11_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN4,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN4
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_11_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_11_IN5,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_11_IN5
    );
  NlwInverterBlock_cnt_9_BUFR_MC_D2_PT_12_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_D2_PT_12_IN0,
      O => NlwInverterSignal_cnt_9_BUFR_MC_D2_PT_12_IN0
    );
  NlwInverterBlock_N_PZ_167_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_167_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_N_PZ_167_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_N_PZ_167_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_167_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_167_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_167_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_N_PZ_167_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_N_PZ_167_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_N_PZ_167_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_167_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_N_PZ_167_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_N_PZ_167_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_167_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_N_PZ_167_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_8_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_8_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_8_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_10_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_10_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_10_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_10_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_10_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_11_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_11_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_11_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_11_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_11_IN5
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_12_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_12_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_12_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_12_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_12_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_12_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_12_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_12_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_12_IN5
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_13_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_13_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_13_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN5
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_13_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN6,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN6
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_13_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN7,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN7
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_13_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_13_IN8,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_13_IN8
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_14_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_14_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_14_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_14_IN8,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_14_IN8
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN7 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN7,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN7
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN8
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure;

