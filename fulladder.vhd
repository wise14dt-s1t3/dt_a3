-- Code belongs to WiSe14DT-S1T3

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity fulladder is
	port (
		a : in std_logic;
		b : in std_logic;
		cin : in std_logic;

		sum : out std_logic;
		cout: out std_logic	
	);--]port
end entity fulladder;

architecture beh of fulladder is
begin

	add:
	process ( a, b, cin ) is
	begin
		sum <= a xor b xor cin;
		cout <= (a and b) or (a and cin) or (b and cin);
	end process add;
	
end architecture beh;
