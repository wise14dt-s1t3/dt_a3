-- Code belongs to WiSe14DT-S1T3

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity rc4dut is
	port (
		did : out std_logic_vector(2 downto 0);
		ok : out std_logic
	);--]port
end entity rc4dut;


architecture beh of rc4dut is
	
	signal did_dut_s	: std_logic_vector( 2 downto 0 ) := (others => '0');
	signal err_dut_s	: std_logic := '0';
	signal cnt_dut_s	: std_logic_vector( 11 downto 0 ) := (others => '0');

	signal did_arop_s	: std_logic_vector( 2 downto 0 ) := (others => '0');
	signal err_arop_s	: std_logic := '0';
	signal cnt_arop_s	: std_logic_vector( 11 downto 0 ) := (others => '0');

	signal valLd_s	: std_logic_vector( 11 downto 0 ) := (others => '0');
	signal nLd_s	: std_logic := '0';
	signal up_s	: std_logic := '0';
	signal down_s	: std_logic := '0';

	signal clk_s	: std_logic := '0';
	signal nres_s	: std_logic := '0';
	
	component sg
		port(
			valLd	: out std_logic_vector( 11 downto 0 );
			nLd	: out std_logic;
			up	: out std_logic;
			down	: out std_logic;
			--
			clk	: out std_logic;
			nres	: out std_logic
		);--]port
	end component sg;
	for all : sg use entity work.sg4counter( beh );
	
	component dut
		port(
			did	: out std_logic_vector( 2 downto 0 );
			err	: out std_logic;
			cnt	: out std_logic_vector( 11 downto 0 );
			--
			valLd	: in std_logic_vector( 11 downto 0 );
			nLd	: in std_logic;
			up	: in std_logic;
			down	: in std_logic;
			--
			clk	: in std_logic;
			nres	: in std_logic
	   );--]port
	end component dut;
	for all : dut use entity work.counter( dut );
	
	component arop
		port(
			did	: out std_logic_vector( 2 downto 0 );
			err	: out std_logic;
			cnt	: out std_logic_vector( 11 downto 0 );
			--
			valLd	: in std_logic_vector( 11 downto 0 );
			nLd	: in std_logic;
			up	: in std_logic;
			down	: in std_logic;
			--
			clk	: in std_logic;
			nres	: in std_logic
	   );--]port
	end component arop;
	for all : arop use entity work.counter( counter_arop );

begin

	did <= did_dut_s;
	
	sg_i : sg
		port map (
			valLd => valLd_s,
			nLd => nLd_s,
			up => up_s,
			down => down_s,
			--
			clk => clk_s,
			nres => nres_s
		)--]port
	;--]sg_i
	
	dut_i : dut
		port map (
			did => did_dut_s,
			err => err_dut_s,
			cnt => cnt_dut_s,
			--
			valLd => valLd_s,
			nLd => nLd_s,
			up => up_s,
			down => down_s,
			--
			clk => clk_s,
			nres => nres_s
		)--]port
	;--}dut_i
	
	arop_i : arop
		port map (
			did => did_arop_s,
			err => err_arop_s,
			cnt => cnt_arop_s,
			--
			valLd => valLd_s,
			nLd => nLd_s,
			up => up_s,
			down => down_s,
			--
			clk => clk_s,
			nres => nres_s
		)--]port
	;--}dut_i

	check:
	process (clk_s) is
		variable ok_v : std_logic := '1';
	begin
		if clk_s='1' and clk_s'event then
			ok_v := err_arop_s xnor err_dut_s;

			for i in 0 to cnt_arop_s'high loop
				if cnt_arop_s(i) /= cnt_dut_s(i)
				then
					ok_v := '0';
				end if;
			end loop;

			ok <= ok_v;
		end if;
	end process check;
	
end architecture beh;

