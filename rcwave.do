onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand /rc4dut/did
add wave -noupdate /rc4dut/ok
add wave -noupdate -divider Error
add wave -noupdate /rc4dut/err_dut_s
add wave -noupdate /rc4dut/err_arop_s
add wave -noupdate -divider Count
add wave -noupdate -expand /rc4dut/cnt_dut_s
add wave -noupdate -expand /rc4dut/cnt_arop_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {226 ns} {1226 ns}
