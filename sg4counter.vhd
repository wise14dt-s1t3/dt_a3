-- Code belongs to WiSe14DT-S1T3

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity sg4counter is
	port (
		valLd	: out std_logic_vector( 11 downto 0 );
		nLd	: out std_logic;
		up	: out std_logic;
		down	: out std_logic;
		--
		clk	: out std_logic;
		nres	: out std_logic
	);
end entity sg4counter;


architecture beh of sg4counter is
	constant fullClockCycle		 : time  := 40 ns;
	constant halfClockCycle		 : time  := fullClockCycle / 2;
	constant oneEigthClockCycle	 : time  := fullClockCycle / 8;
	constant sevenEigthClockCycle   : time  := 7 * oneEigthClockCycle;


	signal   simulationRunning_s	: boolean  := true;

	signal   VDD					: std_logic  := '1';
	signal   GND					: std_logic  := '0';

	signal   nres_s				 : std_logic  := 'L';
	signal   clk_s				  : std_logic;
begin
	VDD <= '1';
	GND <= '0';

	resGen:											 -- RESet GENerator
	process is
	begin
		nres_s <= '0';								  -- set low active reset
		for i in 1 to 2 loop
			wait until '1'=clk_s and clk_s'event;	   -- wait for rising clock edge
		end loop;
		-- since 2 rising clk edges have passed, synchronous reset must have been executed

		wait for oneEigthClockCycle;
		nres_s <= '1';								  -- clear low active reset
		-- 1/8 clk period after rising clk edge reset is vanishing

		wait;
	end process resGen;
	--
	nres <= nres_s;

	clkGen:											 -- CLocK GENerator
	process is
	begin
		clk_s <= '0';
		wait for fullClockCycle;
		while simulationRunning_s loop
			clk_s <= '1';
			wait for halfClockCycle;
			clk_s <= '0';
			wait for halfClockCycle;
		end loop;
		wait;
	end process clkGen;
	--
	clk <= clk_s;

	sg:												 -- Stimuli Generator
	process is
	begin
		simulationRunning_s <= true;

		if  nres_s/='0'  then  wait until nres_s='0';  end if;
		valLd <= (others => '0');
		nLd <= '1';
		up <= '0';
		down <= '0';
		if  nres_s/='1'  then  wait until nres_s='1';  end if;
		--reset has passed

		wait until '1'=clk_s and clk_s'event;	   -- wait for rising clock edge

		-- "HIER" kommt der "eigentliche Test"
		-- Beispielhaft ein paar Testmuster erzeugen

		-- -1
		valLd <= (others => '0');
		nLd <= '1';
		up <= '0';
		down <= '1';
		wait until '1'=clk_s and clk_s'event; -- -1
		wait until '1'=clk_s and clk_s'event; -- -2

		-- up
		valLd <= (others => '0');
		nLd <= '1';
		up <= '1';
		down <= '0';
		wait until '1'=clk_s and clk_s'event; -- -1
		wait until '1'=clk_s and clk_s'event; -- 0
		wait until '1'=clk_s and clk_s'event; -- 1
		wait until '1'=clk_s and clk_s'event; -- 2

		-- Wert setzen
		valLd <= "011111111100"; -- decimal: 2044
		nLd <= '0';
		up <= '0';
		down <= '0';
		wait until '1'=clk_s and clk_s'event;

		-- hoch zählen
		valLd <= (others => '0');
		nLd <= '1';
		up <= '1';
		down <= '0';
		wait until '1'=clk_s and clk_s'event; -- ..1101
		wait until '1'=clk_s and clk_s'event; -- ..1110
		wait until '1'=clk_s and clk_s'event; -- ..1111
		wait until '1'=clk_s and clk_s'event; -- Überlauf

		-- einen Zyklus nichts verändern
		valLd <= (others => '0');
		nLd <= '1';
		up <= '0';
		down <= '0';
		wait until '1'=clk_s and clk_s'event;

		-- und wieder "runter"
		valLd <= (others => '0');
		nLd <= '1';
		up <= '0';
		down <= '1';
		wait until '1'=clk_s and clk_s'event; -- "Unterlauf"

		-- und wieder auf Standardwerte
		valLd <= (others => '0');
		nLd <= '1';
		up <= '0';
		down <= '0';
		-- Der "eigentliche Test" ist nun zu Ende

		-- stop SG after n clk cycles - assuming computed data is stable afterwards
		for  i in 1 to 2 loop
			wait until '1'=clk_s and clk_s'event;	   -- wait for rising clock edge
		end loop;
		simulationRunning_s <= false;				   -- stop clk generation
		--
		wait;
	end process sg;

end architecture;

