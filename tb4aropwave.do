onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb4arop/nres_s
add wave -noupdate /tb4arop/clk_s
add wave -noupdate -divider in
add wave -noupdate -radix decimal /tb4arop/valLd_s
add wave -noupdate /tb4arop/nLd_s
add wave -noupdate /tb4arop/up_s
add wave -noupdate /tb4arop/down_s
add wave -noupdate -divider out
add wave -noupdate /tb4arop/did_s
add wave -noupdate /tb4arop/err_s
add wave -noupdate -radix decimal /tb4arop/cnt_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {693 ns}
