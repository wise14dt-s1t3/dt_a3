-- Code belongs to WiSe14DT-S1T3

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity tb4atop is
end entity tb4atop;


architecture beh of tb4atop is
	
	signal did_s	: std_logic_vector( 2 downto 0 ) := (others => '0');
	signal err_s	: std_logic := '0';
	signal cnt_s	: std_logic_vector( 11 downto 0 ) := (others => '0');

	signal valLd_s	: std_logic_vector( 11 downto 0 ) := (others => '0');
	signal nLd_s	: std_logic := '0';
	signal up_s	: std_logic := '0';
	signal down_s	: std_logic := '0';

	signal clk_s	: std_logic := '0';
	signal nres_s	: std_logic := '0';
	
	component sg
		port(
			valLd	: out std_logic_vector( 11 downto 0 );
			nLd	: out std_logic;
			up	: out std_logic;
			down	: out std_logic;
			--
			clk	: out std_logic;
			nres	: out std_logic
		);--]port
	end component sg;
	for all : sg use entity work.sg4counter( beh );
	
	component dut
		port(
			did	: out std_logic_vector( 2 downto 0 );
			err	: out std_logic;
			cnt	: out std_logic_vector( 11 downto 0 );
			--
			valLd	: in std_logic_vector( 11 downto 0 );
			nLd	: in std_logic;
			up	: in std_logic;
			down	: in std_logic;
			--
			clk	: in std_logic;
			nres	: in std_logic
	   );--]port
	end component dut;
	for all : dut use entity work.counter( Structure );
	
begin
	
	sg_i : sg
		port map (
			valLd => valLd_s,
			nLd => nLd_s,
			up => up_s,
			down => down_s,
			--
			clk => clk_s,
			nres => nres_s
		)--]port
	;--]sg_i
	
	dut_i : dut
		port map (
			did => did_s,
			err => err_s,
			cnt => cnt_s,
			--
			valLd => valLd_s,
			nLd => nLd_s,
			up => up_s,
			down => down_s,
			--
			clk => clk_s,
			nres => nres_s
		)--]port
	;--}dut_i
	
end architecture beh;

